package com.matevskial.dockeradmin.vaadinsecurity;

import com.vaadin.flow.server.HandlerHelper;
import com.vaadin.flow.shared.ApplicationConstants;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Stream;

/**
 * Class that provides static methods  for stuff related to Spring Security
 */
public final class SecurityUtils {
	
	private SecurityUtils() {
		// can't create instances of this class
		// use only static methods
	}
	
	/**
	 * Check whether HttpServletRequest is Vaadin-related request.
	 * 
	 * This method can be used when configuring Spring Security to permit all 
	 * such requests.
	 * 
	 * @param request HttpServiletRequest
	 * @return boolean if request is vaadin internal
	 */
	static boolean isVaadinFrameworkInternalRequest(HttpServletRequest request) {
		final String parameterValue = request.getParameter(
				ApplicationConstants.REQUEST_TYPE_PARAMETER);
		
		return (parameterValue != null &&
				Stream.of(HandlerHelper.RequestType.values())
				.anyMatch(r -> r.getIdentifier().equals(parameterValue)));
	}
}
