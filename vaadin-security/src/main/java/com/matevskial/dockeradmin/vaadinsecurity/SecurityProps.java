package com.matevskial.dockeradmin.vaadinsecurity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "com.matevskial.dockeradmin.security")
public class SecurityProps {

    private String loginProcessingUrl;
    private String loginFailureUrl;
    private String loginUrl;
    private String logoutSuccessUrl;
    private String defaultLoginSuccessUrl;
    private String defaultUsername;
    private String defaultPassword;
    private String defaultRole;
}
