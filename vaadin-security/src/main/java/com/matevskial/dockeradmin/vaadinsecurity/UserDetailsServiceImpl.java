package com.matevskial.dockeradmin.vaadinsecurity;

import com.matevskial.dockeradmin.api.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

	private final UserService userService;
	private final UserDetailsMapper userDetailsMapper;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userService
				.findByUsername(username)
				.map(userDetailsMapper::toUserDetails)
				.orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found", username)));
	}
}
