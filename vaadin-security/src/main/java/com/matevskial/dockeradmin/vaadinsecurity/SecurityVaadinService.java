package com.matevskial.dockeradmin.vaadinsecurity;

import com.matevskial.dockeradmin.api.UserData;
import com.matevskial.dockeradmin.api.UserService;
import com.matevskial.dockeradmin.security.api.SecurityService;
import com.vaadin.flow.server.VaadinServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SecurityVaadinService implements SecurityService {

    private final UserService userService;

    public void logout() {
        SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.logout(VaadinServletRequest.getCurrent().getHttpServletRequest(), null, null);
    }

    @Override
    public boolean isUserLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return authentication != null
                && !(authentication instanceof AnonymousAuthenticationToken)
                && authentication.isAuthenticated();
    }

    @Override
    public Optional<UserData> getLoggedUser() {
        if(!isUserLoggedIn()) {
            return Optional.empty();
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if(principal instanceof String) {
            return userService.findByUsername((String) principal);
        } else if(principal instanceof UserDetails) {
            return userService.findByUsername(((UserDetails) principal).getUsername());
        } else {
            return Optional.empty();
        }
    }
}
