package com.matevskial.dockeradmin.vaadinsecurity;

import com.matevskial.dockeradmin.api.UserData;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
class UserDetailsMapper {

    public UserDetails toUserDetails(UserData userData) {
        return User.withUsername(userData.getUsername())
                .password(userData.getEncodedPassword())
                .roles(userData.getRoles().toArray(new String[0]))
                .build();
    }
}
