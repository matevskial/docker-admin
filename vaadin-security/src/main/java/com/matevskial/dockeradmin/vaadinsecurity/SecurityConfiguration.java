package com.matevskial.dockeradmin.vaadinsecurity;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final SecurityProps securityProps;

	private final UserDetailsService userDetailsService;

	private final PasswordEncoder encoder;

	@Value("${spring.h2.console.path}")
	private String h2ConsoleUrl;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(encoder);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.requestCache().requestCache(new CustomRequestCache())
		.and().authorizeRequests() // turns on authorization
		.requestMatchers(SecurityUtils::isVaadinFrameworkInternalRequest).permitAll() // permits Vaadin internal requests in order for vaadin to work correctly
		
		.anyRequest().authenticated() // allows all authenticated traffic
		.and().formLogin().loginPage(securityProps.getLoginUrl()).permitAll() // configures form-based login
		.loginProcessingUrl(securityProps.getLoginProcessingUrl())
		.failureUrl(securityProps.getLoginFailureUrl())
		.and().logout().logoutSuccessUrl(securityProps.getLogoutSuccessUrl());
	}
	
	/**
	 * Exclude Vaadin static content and  other endpoints from configured Spring security
	 */
	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers(
				"/VAADIN/**",
				"/favicon.ico",
				"/robots.txt",
				"/manifest.webmanifest",
				"/sw.js",
				"/offline.html",
				"/icons/**",
				"/images/**",
				"/styles/**",
				getH2ConsolePathForIgnoring());
	}
	
	private String getH2ConsolePathForIgnoring() {
		return h2ConsoleUrl + (h2ConsoleUrl.endsWith("/") ? "**" : "/**");
	}
}
