package com.matevskial.dockeradmin.vaadinsecurity;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:security.properties")
@EnableConfigurationProperties(SecurityProps.class)
class SecurityPropsConfig {

}
