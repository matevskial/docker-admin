package com.matevskial.dockeradmin.vaadinsecurity;

import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomRequestCache extends HttpSessionRequestCache {

	@Override
	public void saveRequest(HttpServletRequest request, HttpServletResponse response) {
		if(!SecurityUtils.isVaadinFrameworkInternalRequest(request)) {
			super.saveRequest(request, response);			
		}
	}
}
