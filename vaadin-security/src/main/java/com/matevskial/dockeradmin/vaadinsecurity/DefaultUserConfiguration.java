package com.matevskial.dockeradmin.vaadinsecurity;

import com.matevskial.dockeradmin.api.RoleCreateData;
import com.matevskial.dockeradmin.api.RoleService;
import com.matevskial.dockeradmin.api.UserCreateData;
import com.matevskial.dockeradmin.api.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;
import java.util.Set;

@Configuration
@Profile("local")
@Slf4j
@RequiredArgsConstructor
class DefaultUserConfiguration {

    private final SecurityProps securityProps;
    private final UserService userService;
    private final RoleService roleService;

    @PostConstruct
    void setDefaultUser() {
        log.info("Setting default user");
        String roleId =
                roleService.create(RoleCreateData.builder().name(securityProps.getDefaultRole()).build()).getId();
        userService.create(UserCreateData.builder()
                .username(securityProps.getDefaultUsername())
                .password(securityProps.getDefaultPassword())
                .roleIds(Set.of(roleId))
                .build());
    }
}
