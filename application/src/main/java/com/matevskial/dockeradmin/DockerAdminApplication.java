package com.matevskial.dockeradmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerAdminApplication.class, args);
	}

}
