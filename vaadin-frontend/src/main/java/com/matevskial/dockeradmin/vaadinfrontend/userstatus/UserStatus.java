package com.matevskial.dockeradmin.vaadinfrontend.userstatus;

import com.matevskial.dockeradmin.api.UserData;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.contextmenu.HasMenuItems;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.contextmenu.SubMenu;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.menubar.MenuBarVariant;

public class UserStatus extends Composite<MenuBar> {

    private final MenuBar menu = getContent();
    private final MenuItem preferencesMenuItem;
    private final MenuItem logoutMenuItem;

    public UserStatus(UserData userData) {
        menu.addThemeVariants(MenuBarVariant.LUMO_ICON);
        MenuItem userDisplay = createIconItem(menu, VaadinIcon.USER, userData.getUsername());
        SubMenu userSubmenu = userDisplay.getSubMenu();
        preferencesMenuItem = createIconItemForChild(userSubmenu, VaadinIcon.COG, "Preferences");
        userSubmenu.add(new Hr());
        logoutMenuItem = createIconItemForChild(userSubmenu, VaadinIcon.SIGN_OUT, "Log out");
    }


    private MenuItem createIconItem(HasMenuItems menu, VaadinIcon iconName, String label) {
        return createIconItem(menu, iconName, label, false);
    }

    private MenuItem createIconItemForChild(HasMenuItems menu, VaadinIcon iconName, String label) {
        return createIconItem(menu, iconName, label, true);
    }

    private MenuItem createIconItem(HasMenuItems menu, VaadinIcon iconName, String label, boolean isChild) {
        Icon icon = new Icon(iconName);

        if (isChild) {
            icon.getStyle().set("width", "var(--lumo-icon-size-s)");
            icon.getStyle().set("height", "var(--lumo-icon-size-s)");
            icon.getStyle().set("marginRight", "var(--lumo-space-s)");
        }

        MenuItem item = menu.addItem(icon, e -> {
        });

        if (label != null) {
            item.add(new Text(label));
        }

        return item;
    }

    public void setLogoutMenuItemClickListener(ComponentEventListener<ClickEvent<MenuItem>> event) {
        logoutMenuItem.addClickListener(event);
    }

    public void setPreferencesMenuItemClickListener(ComponentEventListener<ClickEvent<MenuItem>> event) {
        preferencesMenuItem.addClickListener(event);
    }
}
