package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.*;
import org.mapstruct.Mapper;

import java.util.HashMap;
import java.util.Map;

@Mapper(componentModel = "spring")
interface DockerContainersAdminViewMapper {

    default DockerContainerCreateFormData toFormData(DockerContainer container) {
        return DockerContainerCreateFormData.builder()
                .containerId(container.getId())
                .name(container.getNames().get(0))
                .image(container.getImageName())
                .portMappings(mapPorts(container))
                .network(!container.getNetworks().isEmpty() ? container.getNetworks().get(0).getName() : null)
                .volumes(mapVolumes(container))
                .environmentVariables(mapEnvironmentVariables(container))
                .build();
    }

    default Map<String, String> mapEnvironmentVariables(DockerContainer container) {
        if(container.getEnvironmentVariables().isEmpty()) {
            return null;
        }
        Map<String, String> environmentVariables = new HashMap<>();
        for(EnvironmentVariable environmentVariable : container.getEnvironmentVariables()) {
            environmentVariables.put(environmentVariable.getName(), environmentVariable.getValue());
        }
        return environmentVariables;
    }

    default Map<String, String> mapVolumes(DockerContainer container) {
        if(container.getVolumes().isEmpty()) {
            return null;
        }
        Map<String, String> volumes = new HashMap<>();
        for(DockerContainerVolume volume : container.getVolumes()) {
            volumes.put(volume.getName(), volume.getDestination());
        }
        return volumes;
    }

    default Map<String, String> mapPorts(DockerContainer container) {
        if(container.getPorts().isEmpty()) {
            return null;
        }
        Map<String, String> ports = new HashMap<>();
        for(DockerContainerPort port : container.getPorts()) {
            ports.put(port.getHostPort(), port.getContainerPort());
        }
        return ports;
    }
}
