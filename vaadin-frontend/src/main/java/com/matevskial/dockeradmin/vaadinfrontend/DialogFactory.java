package com.matevskial.dockeradmin.vaadinfrontend;

import com.matevskial.dockeradmin.vaadinfrontend.dialog.InformationDialogBuilder;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.function.Function;

public class DialogFactory {

    public static Dialog createInformationDialog(String title, String message) {
        Dialog dialog = new InformationDialogBuilder()
                .withTitle(title)
                .withDescription(message)
                .build();
        dialog.setCloseOnOutsideClick(false);
        return dialog;
    }

    public static Dialog createConfirmDialog(String title, String confirmButtonText, Runnable runnable) {
        Dialog dialog = new Dialog();
        dialog.setCloseOnOutsideClick(false);
        createLayoutForConfirmDialog(dialog, title, confirmButtonText, runnable);
        return dialog;
    }

    public static Dialog createConfirmDialogWithCheckbox(String title, String checkboxText, String confirmButtonText, Function<Boolean, Void> function) {
        Dialog dialog = new Dialog();
        dialog.setCloseOnOutsideClick(false);
        createLayoutForConfirmDialogWithCheckbox(dialog, title, checkboxText, confirmButtonText, function);
        return dialog;
    }

    private static void createLayoutForConfirmDialogWithCheckbox(Dialog dialog, String title, String checkboxText, String confirmButtonText, Function<Boolean, Void> function) {
        H2 headline = new H2(title);
        headline.addClassName("dialog-headline");

        Checkbox checkBox = new Checkbox(checkboxText);

        Button cancelButton = new Button("Cancel");
        cancelButton.addClickListener(e -> dialog.close());
        Button confirmButton = new Button(confirmButtonText);
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        confirmButton.addClickListener(event -> {
            indicateRunning(confirmButton);
            function.apply(checkBox.getValue());
            dialog.close();
        });

        HorizontalLayout buttonsHorizontalLayout = new HorizontalLayout(confirmButton, cancelButton);
        buttonsHorizontalLayout.setPadding(false);

        VerticalLayout dialogLayout = new VerticalLayout(headline, checkBox, buttonsHorizontalLayout);
        dialogLayout.setPadding(false);
        dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);
        dialogLayout.setAlignSelf(FlexComponent.Alignment.END, buttonsHorizontalLayout);
        dialog.add(dialogLayout);
    }

    private static void createLayoutForConfirmDialog(Dialog dialog, String title, String confirmButtonText, Runnable runnable) {
        H2 headline = new H2(title);
        headline.addClassName("dialog-headline");

        Button cancelButton = new Button("Cancel");
        cancelButton.addClickListener(e -> dialog.close());
        Button confirmButton = new Button(confirmButtonText);
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        confirmButton.addClickListener(event -> {
            indicateRunning(confirmButton);
            runnable.run();
            dialog.close();
        });

        HorizontalLayout buttonsHorizontalLayout = new HorizontalLayout(confirmButton, cancelButton);
        buttonsHorizontalLayout.setPadding(false);

        VerticalLayout dialogLayout = new VerticalLayout(headline, buttonsHorizontalLayout);
        dialogLayout.setPadding(false);
        dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);
        dialogLayout.setAlignSelf(FlexComponent.Alignment.END, buttonsHorizontalLayout);
        dialog.add(dialogLayout);
    }

    private static void indicateRunning(Button confirmButton) {
        confirmButton.setText("Running");
        confirmButton.setEnabled(false);
    }
}
