package com.matevskial.dockeradmin.vaadinfrontend;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Div;

public class HintText extends Composite<Div> {

    public HintText(String text) {
        Div hintDiv = getContent();
        hintDiv.setText(text);
        hintDiv.getStyle()
                .set("padding", "var(--lumo-size-s)")
                .set("text-align", "center").set("font-style", "italic")
                .set("color", "var(--lumo-contrast-70pct)");
    }
}
