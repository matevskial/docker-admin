package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerContainer;
import com.matevskial.dockeradmin.api.docker.DockerContainerLogsService;
import com.matevskial.dockeradmin.api.docker.logs.DockerContainerLogsRequest;
import com.matevskial.dockeradmin.vaadinfrontend.LogsArea;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.List;
import java.util.Objects;

class DockerContainerLogsViewer extends VerticalLayout {

    private static final int MAX_LOG_LINES = 200;

    private final DockerContainerLogsService logsService;
    private final DockerContainer container;
    private ComboBox<String> numberOfLinesToTailComboBox;
    private Button getLogsButton;
    private LogsArea logsArea;

    public DockerContainerLogsViewer(DockerContainerLogsService logsService, DockerContainer container) {
        addClassName("log-viewer");
        this.logsService = logsService;
        this.container = container;
        configureNumberOfLinesToTailComboBox();
        configureGetLogsButton();
        configureLogsArea();
        add(createToolBar());
        add(logsArea);
    }

    private void configureLogsArea() {
        logsArea = new LogsArea();
        logsArea.setReadOnly(true);
        logsArea.setWidthFull();
        logsArea.setMaxHeight("300px");
    }

    private HorizontalLayout createToolBar() {
        HorizontalLayout toolbarHorizontalLayout = new HorizontalLayout(numberOfLinesToTailComboBox, getLogsButton);
        toolbarHorizontalLayout.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
        return toolbarHorizontalLayout;
    }

    private void configureGetLogsButton() {
        getLogsButton = new Button("Get logs");
        getLogsButton.setIcon(VaadinIcon.REFRESH.create());
        getLogsButton.addClickListener(event -> {
            logsArea.setValue(logsService.getLogs(getLogsRequest()));
        });
    }

    private DockerContainerLogsRequest getLogsRequest() {
        return DockerContainerLogsRequest.builder()
                .containerId(container.getId())
                .numberOfLinesToTail(getNumberOfLinesToTail())
                .build();
    }

    private int getNumberOfLinesToTail() {
        String numberOfLinesToTailStr = Objects.requireNonNullElse(numberOfLinesToTailComboBox.getValue(), "");
        try {
            return Integer.parseInt(numberOfLinesToTailStr);
        } catch (NumberFormatException e) {
            return MAX_LOG_LINES;
        }
    }

    private void configureNumberOfLinesToTailComboBox() {
        numberOfLinesToTailComboBox = new ComboBox<>("Number of lines to tail");
        numberOfLinesToTailComboBox.setItems(List.of("10", "50", "100", "All"));
        numberOfLinesToTailComboBox.setValue("10");
    }
}
