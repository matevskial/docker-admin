package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.github.appreciated.card.Card;
import com.matevskial.dockeradmin.api.docker.DockerContainer;
import com.matevskial.dockeradmin.api.docker.DockerContainerLogsService;
import com.matevskial.dockeradmin.api.docker.DockerContainerService;
import com.matevskial.dockeradmin.vaadinfrontend.Clipboard;
import com.matevskial.dockeradmin.vaadinfrontend.DialogFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.dom.ElementFactory;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;

import java.util.List;
import java.util.Objects;

class DockerContainersList extends VerticalLayout {

    private final DockerContainerService dockerContainerService;
    private final DockerContainerLogsService logsService;

    public DockerContainersList(DockerContainerService dockerContainerService, DockerContainerLogsService logsService) {
        this.dockerContainerService = dockerContainerService;
        this.logsService = logsService;
        setPadding(false);
        setSpacing(false);
    }

    public void setContainers(List<DockerContainer> containers) {
        removeAll();
        containers.forEach(container -> add(new VerticalLayout(createDockerContainerCard(container))));
    }

    private Component createDockerContainerCard(DockerContainer container) {
        Card card = new Card(createCardLayout(container));
        card.setWidth("100%");
        return card;
    }

    private Component createCardLayout(DockerContainer container) {
        Avatar containerStatusAvatar = createContainerRunningStatusAvatar(container);
        HorizontalLayout cardLayout = new HorizontalLayout(containerStatusAvatar, createContainerLayout(container));
        cardLayout.setWidthFull();
        cardLayout.setMargin(true);
        return cardLayout;
    }

    private Component createContainerLayout(DockerContainer container) {
        VerticalLayout dockerContainerLayout = new VerticalLayout();
        dockerContainerLayout.setSpacing(false);
        dockerContainerLayout.setPadding(false);
        dockerContainerLayout.getElement().appendChild(ElementFactory.createStrong(container.getNames().toString()));
        dockerContainerLayout.add(createContainerIdAndStatus(container));
        dockerContainerLayout.add(createActionButtons(container));
        dockerContainerLayout.add(new DockerContainerDetails(container, logsService));
        return dockerContainerLayout;
    }

    private Component createContainerIdAndStatus(DockerContainer container) {
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.add(createSecondaryText(String.format("ID: %s", container.getId())));
        layout.add(createSecondaryText(String.format("Status: %s", container.getStatus())));
        layout.add(createImageTextDisplay(container.getImageName()));
        layout.add(createSecondaryText(String.format("Created at: %s", container.getCreatedAt())));
        return layout;
    }

    private Component createActionButtons(DockerContainer container) {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setPadding(false);

        Button startContainerButton = new Button();
        startContainerButton.setIcon(VaadinIcon.PLAY_CIRCLE.create());
        startContainerButton.getElement().setAttribute("title", "Start this container");
        startContainerButton.setEnabled(container.getRunningState().equals(DockerContainer.RunningState.EXITED));
        startContainerButton.addClickListener(event -> {
            DialogFactory.createConfirmDialog("Start container " + container.getNames(), "Start container", () -> {
                String errorMessage = dockerContainerService.startContainer(container.getId());
                if(errorMessage != null) {
                    Dialog dialog = DialogFactory.createInformationDialog("Error starting container", errorMessage);
                    dialog.open();
                } else {
                    fireEvent(new StartContainerEvent(this));
                }
            }).open();
        });

        Button stopContainerButton = new Button();
        stopContainerButton.getElement().setAttribute("title", "Stop this container");
        stopContainerButton.setIcon(VaadinIcon.STOP.create());
        stopContainerButton.setEnabled(container.getRunningState().equals(DockerContainer.RunningState.RUNNING));
        stopContainerButton.addClickListener(event -> {
            DialogFactory.createConfirmDialog("Stop container " + container.getNames(), "Stop container", () -> {
                String errorMessage = dockerContainerService.stopContainer(container.getId());
                if(errorMessage != null) {
                    Dialog dialog = DialogFactory.createInformationDialog("Error stopping container", errorMessage);
                    dialog.open();
                } else {
                    fireEvent(new StopContainerEvent(this));
                }
            }).open();

        });

        Button duplicateContainerButton = new Button();
        duplicateContainerButton.getElement().setAttribute("title", "Duplicate this container");
        duplicateContainerButton.setIcon(VaadinIcon.COPY.create());
        duplicateContainerButton.addClickListener(event -> {
            fireEvent(new DuplicateContainerEvent(this, container));
        });

        Button copyCommandLineButton = new Button();
        copyCommandLineButton.getElement().setAttribute("title", "Copy 'docker run' command for this container");
        copyCommandLineButton.setIcon(VaadinIcon.TERMINAL.create());
        copyCommandLineButton.addClickListener(event -> Clipboard.setText(dockerContainerService.buildDockerRunCommand(container.getId())));

        Button deleteContainerButton = new Button();
        deleteContainerButton.getElement().setAttribute("title", "Delete this container");
        deleteContainerButton.setIcon(VaadinIcon.TRASH.create());
        deleteContainerButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteContainerButton.setEnabled(container.getRunningState().equals(DockerContainer.RunningState.EXITED));
        deleteContainerButton.addClickListener(event -> {
            DialogFactory.createConfirmDialogWithCheckbox("Delete container " + container.getNames(), "Also delete volumes associated with this container", "Delete container", (deleteVolumes) -> {
                String errorMessage = dockerContainerService.deleteContainer(container.getId(), Objects.requireNonNullElse(deleteVolumes, false));
                if(errorMessage != null) {
                    Dialog dialog = DialogFactory.createInformationDialog("Error deleting container", errorMessage);
                    dialog.open();
                } else {
                    fireEvent(new DeleteContainerEvent(this));
                }
                return null;
            }).open();
        });

        layout.add(startContainerButton, stopContainerButton, duplicateContainerButton, copyCommandLineButton, deleteContainerButton);
        return layout;
    }

    private Component createSecondaryText(String text) {
        Span span = new Span(text);
        span.addClassName("secondary-text");
        return span;
    }

    private Component createImageTextDisplay(String imageName) {
        Icon icon = VaadinIcon.LINK.create();
        icon.getStyle()
                .set("padding", "var(--lumo-space-xs");
//                .set("box-sizing", "border-box");
        Span link = new Span(icon);
        link.getElement().getThemeList().add("badge small pill");
        link.addClickListener(event -> {
            String location = RouteConfiguration.forSessionScope().getUrl(DockerImagesAdminView.class);
            getUI().ifPresent(
                    ui -> ui.navigate(location, QueryParameters.fromString(String.format("name=%s", imageName))));
        });
        Span span = new Span(new Text(String.format("Image: %s  ", imageName)), link);
        span.addClassName("secondary-text");
        return span;
    }

    private Avatar createContainerRunningStatusAvatar(DockerContainer container) {
        Avatar runningStatusAvatar = new Avatar(container.getRunningState().name());
        if(container.getRunningState().equals(DockerContainer.RunningState.RUNNING)) {
            runningStatusAvatar.addClassName("avatar-running-container");
        } else {
            runningStatusAvatar.addClassName("avatar-exited-container");
        }
        return runningStatusAvatar;
    }

    public static abstract class DockerContainersListEvent extends ComponentEvent<DockerContainersList> {

        protected DockerContainersListEvent(DockerContainersList source) {
            super(source, false);
        }
    }

    public static class DuplicateContainerEvent extends DockerContainersListEvent {
        @Getter
        private final DockerContainer container;

        protected DuplicateContainerEvent(DockerContainersList source, DockerContainer container) {
            super(source);
            this.container = container;
        }
    }
    public static class StartContainerEvent extends DockerContainersListEvent {

        protected StartContainerEvent(DockerContainersList source) {
            super(source);
        }
    }

    public static class StopContainerEvent extends DockerContainersListEvent {

        protected StopContainerEvent(DockerContainersList source) {
            super(source);
        }
    }

    public static class DeleteContainerEvent extends DockerContainersListEvent {

        protected DeleteContainerEvent(DockerContainersList source) {
            super(source);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
