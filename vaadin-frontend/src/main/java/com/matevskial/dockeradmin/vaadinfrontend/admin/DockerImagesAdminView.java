package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerImageService;
import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;

import java.util.List;
import java.util.Map;

@Route(value = "images", layout = DockerAdminLayout.class)
class DockerImagesAdminView extends VerticalLayout implements BeforeEnterObserver {

    private final DockerImageService dockerImageService;
    private Scroller tabContent;
    private DockerImagesListView dockerImagesListView;
    private SearchImagesOnDockerHubView searchImagesOnDockerHubView;
    private Tabs tabs;
    private Tab dockerImagesListViewTab;
    private Tab searchImagesOnDockerHubTab;

    public DockerImagesAdminView(DockerImageService dockerImageService) {
        this.dockerImageService = dockerImageService;
        configureTabContent();
        configureTabs();
        configureLocalDockerImagesView();
        configureSearchImagesOnDockerHubView();
        add(tabs);
        add(tabContent);
        populateTabContent(tabs.getSelectedTab());
    }

    private void configureTabContent() {
        tabContent = new Scroller();
        tabContent.setWidthFull();
    }

    private void configureTabs() {
        dockerImagesListViewTab = new Tab("Local images");
        searchImagesOnDockerHubTab = new Tab("Images on Docker hub");
        tabs = new Tabs(dockerImagesListViewTab, searchImagesOnDockerHubTab);
        tabs.addSelectedChangeListener(event -> populateTabContent(event.getSelectedTab()));
    }

    private void populateTabContent(Tab selectedTab) {
        if(dockerImagesListViewTab.equals(selectedTab)) {
            tabContent.setContent(dockerImagesListView);
        } else if(searchImagesOnDockerHubTab.equals(selectedTab)) {
            tabContent.setContent(searchImagesOnDockerHubView);
        }
    }

    private void configureLocalDockerImagesView() {
        dockerImagesListView = new DockerImagesListView(dockerImageService);
    }

    private void configureSearchImagesOnDockerHubView() {
        searchImagesOnDockerHubView = new SearchImagesOnDockerHubView();
        searchImagesOnDockerHubView.addSearchImagesButtonClickListener(event -> {
            String searchTerm = searchImagesOnDockerHubView.getSearchTerm();
            if(searchTerm != null && !searchTerm.isEmpty() && !searchTerm.isBlank()) {
                searchImagesOnDockerHubView.setItems(dockerImageService.searchImages(searchTerm));
            }
        });
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters
                .getParameters();
        String imageName = parametersMap.getOrDefault("name", List.of("")).get(0);
        dockerImagesListView.setItemsAndFields(DockerImageFilter.builder()
                .repoTag(imageName)
                .build());
    }
}
