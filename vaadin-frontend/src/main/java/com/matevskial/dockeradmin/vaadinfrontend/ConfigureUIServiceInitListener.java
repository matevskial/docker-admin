package com.matevskial.dockeradmin.vaadinfrontend;

import com.matevskial.dockeradmin.security.api.SecurityService;
import com.matevskial.dockeradmin.vaadinfrontend.login.LoginView;
import com.matevskial.dockeradmin.vaadinsecurity.SecurityProps;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Wire Spring security with Vaadin navigation system
 *
 */
@Component
@RequiredArgsConstructor
public class ConfigureUIServiceInitListener implements VaadinServiceInitListener {

	private final SecurityService securityService;

	private final SecurityProps securityProps;

	/*
	 * Listen for the initialization of UI(internal root component in Vaadin)
	 */
	@Override
	public void serviceInit(ServiceInitEvent event) {
		event.getSource().addUIInitListener(uiEvent -> {
			final UI ui = uiEvent.getUI();
			ui.addBeforeEnterListener(this::authenticateNavigation);
		});

	}
	
	/**
	 * BeforeEnterEvent that is used to reroute all requests to LoginView 
	 * if the user is not logged in.
	 * @param event before enter event
	 */
	private void authenticateNavigation(BeforeEnterEvent event) {
		if(!LoginView.class.equals(event.getNavigationTarget())
				&& !securityService.isUserLoggedIn()) {
			event.rerouteTo(securityProps.getLoginUrl());
		} else if(LoginView.class.equals(event.getNavigationTarget()) && securityService.isUserLoggedIn()) {
			event.rerouteTo(securityProps.getDefaultLoginSuccessUrl());
		}
	}
}
