package com.matevskial.dockeradmin.vaadinfrontend.home;

import com.github.appreciated.card.RippleClickableCard;
import com.github.appreciated.card.content.Item;
import com.matevskial.dockeradmin.api.docker.DockerException;
import com.matevskial.dockeradmin.api.docker.DockerPingService;
import com.matevskial.dockeradmin.vaadinfrontend.MainLayout;
import com.matevskial.dockeradmin.vaadinfrontend.admin.DockerAdminDefaultRoute;
import com.matevskial.dockeradmin.vaadinfrontend.dialog.InformationDialogBuilder;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value="", layout = MainLayout.class)
@PageTitle("Docker Admin")
public class HomeView extends VerticalLayout {

    private final DockerPingService dockerPingService;

    public HomeView(DockerPingService dockerPingService) {
        this.dockerPingService = dockerPingService;
        H3 title = new H3("Connect to docker instance");
        add(title);

        RippleClickableCard card1 = new RippleClickableCard(new Item("Local docker instance", "The docker instance running on your machine"));
        card1.addClickListener(e -> rerouteToAdminViewIfConnectionToDockerIsSuccessful());
        HorizontalLayout cards = new HorizontalLayout(card1);
        add(cards);
    }

    private void rerouteToAdminViewIfConnectionToDockerIsSuccessful() {
        try {
            dockerPingService.ping();
            getUI().ifPresent(ui -> ui.navigate(DockerAdminDefaultRoute.class));
            System.out.println("successful");
        } catch (DockerException e) {
            e.printStackTrace();
            Dialog unableToConnectDialog = new InformationDialogBuilder()
                    .withTitle("Error")
                    .withDescription(e.getMessage())
                    .build();
            unableToConnectDialog.setCloseOnOutsideClick(false);
            unableToConnectDialog.open();
        }
    }
}
