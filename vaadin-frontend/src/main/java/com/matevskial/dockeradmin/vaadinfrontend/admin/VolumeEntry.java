package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerVolume;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBoxVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

class VolumeEntry extends HorizontalLayout {

    private final ComboBox<String> volumeComboBox;
    private final ComboBox<String> pathInContainerComboBox;
    private final Button clearButton;

    public VolumeEntry(List<DockerVolume> volumes, List<String> pathsInContainer) {
        this(volumes, pathsInContainer, "", "");
    }

    public VolumeEntry(List<DockerVolume> volumes, List<String> pathsInContainers, String key, String value) {

        setPadding(false);
        setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);

        clearButton = new com.vaadin.flow.component.button.Button();
        clearButton.setIcon(VaadinIcon.CLOSE_SMALL.create());
        clearButton.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY_INLINE);
        Span clearButtonBadge = new Span(clearButton);
        clearButtonBadge.getElement().getThemeList().add("badge  small");

        volumeComboBox = new ComboBox<>("Name");
        volumeComboBox.getStyle().set("--vaadin-combo-box-overlay-width", "350px");
        volumeComboBox.setItems(volumes.stream().map(DockerVolume::getName).collect(Collectors.toList()));
        volumeComboBox.setValue(key);
        volumeComboBox.addThemeVariants(ComboBoxVariant.LUMO_SMALL);
        volumeComboBox.setAllowCustomValue(true);
        volumeComboBox.addCustomValueSetListener(event -> volumeComboBox.setValue(event.getDetail()));

        pathInContainerComboBox = new ComboBox<>("Value");
        pathInContainerComboBox.getStyle().set("--vaadin-combo-box-overlay-width", "350px");
        pathInContainerComboBox.setAllowCustomValue(true);
        pathInContainerComboBox.addCustomValueSetListener(event -> pathInContainerComboBox.setValue(event.getDetail()));
        pathInContainerComboBox.setItems(pathsInContainers);
        pathInContainerComboBox.setValue(value);
        pathInContainerComboBox.addThemeVariants(ComboBoxVariant.LUMO_SMALL);
        add(volumeComboBox, pathInContainerComboBox, clearButtonBadge);
    }

    public void setPathsInContainer(List<String> pathsInContainer) {
        String oldValue = pathInContainerComboBox.getValue();
        pathInContainerComboBox.setItems(pathsInContainer);
        pathInContainerComboBox.setValue(oldValue);
    }

    public String getVolume() {
        return volumeComboBox.getValue();
    }

    public String getPathInContainer() {
        return pathInContainerComboBox.getValue();
    }

    public void removeFromParent() {
        getElement().removeFromParent();
    }

    public void addClearButtonClickListener(ComponentEventListener<ClickEvent<Button>> eventListener) {
        clearButton.addClickListener(eventListener);
    }

    public boolean hasValue() {
        return StringUtils.hasText(getVolume()) && StringUtils.hasText(getPathInContainer());
    }
}
