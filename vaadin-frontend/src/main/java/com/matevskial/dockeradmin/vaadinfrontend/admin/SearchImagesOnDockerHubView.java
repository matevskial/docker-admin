package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerImage;
import com.matevskial.dockeradmin.api.docker.DockerImageSearchItem;
import com.matevskial.dockeradmin.vaadinfrontend.ButtonFactory;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;

import java.util.List;

class SearchImagesOnDockerHubView extends VerticalLayout {

    private Grid<DockerImageSearchItem> searchItemsGrid;
    private TextField searchTermTextField;
    private Button searchImagesButton;

    public SearchImagesOnDockerHubView() {
        configureToolbarComponents();
        configureSearchItemsGrid();
        add(createToolbar());
        add(searchItemsGrid);
    }

    private Component createToolbar() {
        HorizontalLayout layout = new HorizontalLayout(searchTermTextField, searchImagesButton);
        layout.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
        return layout;
    }

    private void configureSearchItemsGrid() {
        searchItemsGrid = new Grid<>(DockerImageSearchItem.class, false);
        searchItemsGrid.addColumn(new ComponentRenderer<>(dockerImageSearchItem -> {
            Span nameSpan = new Span(dockerImageSearchItem.getName());
            Span descriptionSpan = new Span(dockerImageSearchItem.getDescription());
            descriptionSpan.addClassName("secondary-text");
            VerticalLayout layout = new VerticalLayout(nameSpan, descriptionSpan);
            layout.setPadding(false);
            layout.setSpacing(false);
            return layout;
        })).setHeader("Name").setAutoWidth(true).setFlexGrow(0);
        searchItemsGrid.addColumn(new ComponentRenderer<>(dockerImageSearchItem -> {
            if(dockerImageSearchItem.isOfficial()) {
                Span officialSpan = new Span("Official image");
                officialSpan.getElement().getThemeList().add("badge success pill");
                return officialSpan;
            } else {
                Span notOfficialSpan = new Span("Not official image");
                notOfficialSpan.getElement().getThemeList().add("badge error pill");
                return notOfficialSpan;
            }
        })).setHeader("Official status").setAutoWidth(true).setFlexGrow(0);
        searchItemsGrid.addColumn(DockerImageSearchItem::getStarCount).setHeader("Stars").setAutoWidth(true).setFlexGrow(0);
        searchItemsGrid.addColumn(new ComponentRenderer<>(dockerImageSearchItem -> {
            return ButtonFactory.createLinkButton(null, DockerImage.getDockerHubLink(dockerImageSearchItem.getName()));
        })).setHeader("Docker hub").setAutoWidth(true).setFlexGrow(0);
        searchItemsGrid.addColumn(new ComponentRenderer<>(dockerImageSearchItem -> ButtonFactory.createCopyTextButton("Copy image pull command", String.format("docker pull %s:%s", dockerImageSearchItem.getName(), "latest")))).setHeader("Docker pull command");
    }

    private void configureToolbarComponents() {
        searchTermTextField = new TextField("Search term");
        searchTermTextField.setClearButtonVisible(true);
        searchImagesButton = new Button("Search");
        searchImagesButton.setIcon(VaadinIcon.SEARCH.create());
        searchImagesButton.addClickShortcut(Key.ENTER);
    }

    public void addSearchImagesButtonClickListener(ComponentEventListener<ClickEvent<Button>> eventListener) {
        searchImagesButton.addClickListener(eventListener);
    }

    public void setItems(List<DockerImageSearchItem> items) {
        searchItemsGrid.setItems(items);
    }

    public String getSearchTerm() {
        return searchTermTextField.getValue();
    }
}
