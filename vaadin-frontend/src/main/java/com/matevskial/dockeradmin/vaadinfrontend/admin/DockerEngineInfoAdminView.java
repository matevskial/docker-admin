package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerInfo;
import com.matevskial.dockeradmin.api.docker.DockerInfoService;
import com.matevskial.dockeradmin.vaadinfrontend.datafields.DataFieldsLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H6;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route(value = "engine-info", layout = DockerAdminLayout.class)
class DockerEngineInfoAdminView extends VerticalLayout {

    private final DockerInfo dockerInfo;

    public DockerEngineInfoAdminView(DockerInfoService dockerInfoService) {
        dockerInfo = dockerInfoService.getInfo();
        add(createDockerDetails());
        add(createSystemDetails());
    }

    private Component createDockerDetails() {
        DataFieldsLayout dataFieldsLayout1 = new DataFieldsLayout();
        dataFieldsLayout1.addDataField("Server version", dockerInfo.getDockerServerVersion());
        dataFieldsLayout1.addDataField("Docker root directory", dockerInfo.getDockerRootDirectory());

        DataFieldsLayout pluginsDataFieldLayout = new DataFieldsLayout();
        pluginsDataFieldLayout.addListDataField("Volume", dockerInfo.getVolumePlugins());
        pluginsDataFieldLayout.addListDataField("Network", dockerInfo.getNetworkPlugins());
        Details pluginsDetails = new Details("Plugins", pluginsDataFieldLayout);
        pluginsDetails.addThemeVariants(DetailsVariant.SMALL);

        DataFieldsLayout statisticsDataFieldLayout = new DataFieldsLayout();
        statisticsDataFieldLayout.addDataField("Containers", String.valueOf(dockerInfo.getContainers()));
        statisticsDataFieldLayout.addDataField("Running containers", String.valueOf(dockerInfo.getRunningContainers()));
        statisticsDataFieldLayout.addDataField("Exited containers", String.valueOf(dockerInfo.getExitedContainers()));
        statisticsDataFieldLayout.addDataField("Images", String.valueOf(dockerInfo.getImages()));
        Details statisticsDetails = new Details("Statistics", statisticsDataFieldLayout);
        statisticsDetails.addThemeVariants(DetailsVariant.SMALL);

        VerticalLayout detailsContentLayout = new VerticalLayout(dataFieldsLayout1, pluginsDetails, statisticsDetails);
        detailsContentLayout.setPadding(false);
        detailsContentLayout.setSpacing(false);
        Details details = new Details();
        details.setSummary(createLargeSummaryComponent("Docker"));
        details.setContent(detailsContentLayout);
        details.setWidthFull();
        details.setOpened(true);
        return details;
    }

    private Component createSystemDetails() {
        DataFieldsLayout softwareDataFieldsLayout = new DataFieldsLayout();
        softwareDataFieldsLayout.addDataField("Operating system", dockerInfo.getOperatingSystem());
        softwareDataFieldsLayout.addDataField("OS type", dockerInfo.getOsType());
        softwareDataFieldsLayout.addDataField("Kernel version", dockerInfo.getKernelVersion());
        softwareDataFieldsLayout.addDataField("Hostname", dockerInfo.getHostname());

        DataFieldsLayout hardwareDataFieldsLayout = new DataFieldsLayout();
        hardwareDataFieldsLayout.addDataField("Architecture", dockerInfo.getArchitecture());
        hardwareDataFieldsLayout.addDataField("Number of CPUs(cores + threads)", String.valueOf(dockerInfo.getNumberOfCpus()));
        hardwareDataFieldsLayout.addDataField("Memory", dockerInfo.getMemoryInGigaBytes());

        VerticalLayout detailsContentLayout = new VerticalLayout(
                createSmallSummaryComponent("Software"),
                softwareDataFieldsLayout,
                createSmallSummaryComponent("Hardware"),
                hardwareDataFieldsLayout
        );
        detailsContentLayout.setPadding(false);
        detailsContentLayout.setSpacing(false);
        Details details = new Details();
        details.setSummary(createLargeSummaryComponent("System"));
        details.setContent(detailsContentLayout);
        details.setWidthFull();
        details.setOpened(true);
        return details;
    }

    private Component createLargeSummaryComponent(String summaryText) {
        VerticalLayout verticalLayout = new VerticalLayout(createLargeSummaryTextComponent(summaryText), createSummaryHr());
        verticalLayout.setSpacing(false);
        verticalLayout.setPadding(false);
        return verticalLayout;
    }

    private Component createSmallSummaryComponent(String summaryText) {
        VerticalLayout verticalLayout = new VerticalLayout(createSmallSummaryTextComponent(summaryText), createSummaryHr());
        verticalLayout.setSpacing(false);
        verticalLayout.setPadding(false);
        verticalLayout.setSizeUndefined();
        return verticalLayout;
    }

    private Component createLargeSummaryTextComponent(String summaryText) {
        H2 h2 = new H2(summaryText);
        h2.getStyle().set("color", "var(--lumo-secondary-text-color)");
        return h2;
    }

    private Component createSmallSummaryTextComponent(String summaryText) {
        H6 summaryTextElement = new H6(new Text(summaryText));
        summaryTextElement.getStyle().set("color", "var(--lumo-secondary-text-color)");
        return summaryTextElement;
    }

    private Component createSummaryHr() {
        Hr hr = new Hr();
        hr.addClassName("details-summary-hr");
        return hr;
    }
}
