package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerNetworkCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerNetworkCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerNetworkService;
import com.matevskial.dockeradmin.vaadinfrontend.dialog.ActionDialogBuilder;
import com.matevskial.dockeradmin.vaadinfrontend.keyvaluelistcustomfield.KeyValueListCustomField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

import java.util.List;

public class DockerNetworkCreateForm extends FormLayout {

    private final DockerNetworkService dockerNetworkService;
    private final TextField name = new TextField("Name");
    private final Select<String> driver = new Select<>();
    private final KeyValueListCustomField driverOptions = new KeyValueListCustomField("Driver options", "=");
    private final TextField subnet = new TextField("Subnet");
    private final TextField gateway = new TextField("Gateway");
    private final Button createButton = new Button("Create");
    private final Button resetButton = new Button("Reset");

    private final Binder<DockerNetworkCreateFormData> binder = new BeanValidationBinder<>(DockerNetworkCreateFormData.class);

    public DockerNetworkCreateForm(DockerNetworkService dockerNetworkService) {
        this.dockerNetworkService = dockerNetworkService;

        binder.bindInstanceFields(this);

        setResponsiveSteps(new ResponsiveStep("0", 1));
        driver.setLabel("Driver");
        driver.setItems(List.of("bridge", "ipvlan", "macvlan", "overlay"));
        add(name, driver, driverOptions, subnet, gateway, createButtonsLayout());
    }

    private Component createButtonsLayout() {
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        createButton.addClickListener(event -> {
            DockerNetworkCreateFormData formData = new DockerNetworkCreateFormData();
            try {
                binder.writeBean(formData);
                showDialog(dockerNetworkService.createNetwork(formData));
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        });

        resetButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        resetButton.addClickListener(event -> {
            Dialog dialog = new ActionDialogBuilder()
                    .withTitle("Reset all fields?")
                    .withConfirmButtonText("Reset")
                    .withCancelButtonText("Cancel")
                    .withConfirmButtonAction((dialog1, event1) -> {
                        resetFields();
                        dialog1.close();
                        return null;
                    })
                    .build();
            dialog.open();
        });

        return new HorizontalLayout(createButton, resetButton);
    }

    private void resetFields() {
        name.clear();
        driver.clear();
        driverOptions.clear();
        subnet.clear();
        gateway.clear();
    }

    private void showDialog(DockerNetworkCreateResponse networkCreateResponse) {
        Dialog dialog;
        if(networkCreateResponse.isError()) {
            dialog = new ActionDialogBuilder()
                    .withTitle("Error creating network")
                    .withDescription(networkCreateResponse.getErrorMessage())
                    .withCancelButtonText("Return to form")
                    .build();
        } else {
            dialog = new ActionDialogBuilder()
                    .withTitle("Network created")
                    .withDescription(String.format("Id of created network: %s", networkCreateResponse.getNetworkId()))
                    .withConfirmButtonText("Return to networks list")
                    .withConfirmButtonAction((dialog1, event) -> {
                        fireEvent(new NetworkCreatedEvent(this));
                        dialog1.close();
                        return null;
                    })
                    .withCancelButtonText("Return to form")
                    .build();
        }
        dialog.open();
    }

    public static abstract class DockerNetworkCreateFormEvent extends ComponentEvent<DockerNetworkCreateForm> {

        protected DockerNetworkCreateFormEvent(DockerNetworkCreateForm source) {
            super(source, false);
        }
    }

    public static class NetworkCreatedEvent extends DockerNetworkCreateFormEvent {

        protected NetworkCreatedEvent(DockerNetworkCreateForm source) {
            super(source);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
