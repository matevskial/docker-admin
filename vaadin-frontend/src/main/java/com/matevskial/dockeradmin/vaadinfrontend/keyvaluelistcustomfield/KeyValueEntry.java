package com.matevskial.dockeradmin.vaadinfrontend.keyvaluelistcustomfield;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import org.springframework.util.StringUtils;


class KeyValueEntry extends HorizontalLayout {

    private final TextField keyTextField;
    private final TextField valueTextField;
    private final Button clearButton;

    public KeyValueEntry() {
        this("", "", "");
    }

    public KeyValueEntry(String key, String value) {
        this(key, value, "");
    }

    public KeyValueEntry(String delimiter) {
        this("", "", delimiter);
    }

    public KeyValueEntry(String key, String value, String delimiter) {
        setPadding(false);
        setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);

        clearButton = new com.vaadin.flow.component.button.Button();
        clearButton.setIcon(VaadinIcon.CLOSE_SMALL.create());
        clearButton.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY_INLINE);
        Span clearButtonBadge = new Span(clearButton);
        clearButtonBadge.getElement().getThemeList().add("badge  small");

        keyTextField = new TextField("Name");
        keyTextField.setValue(key);
        keyTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        valueTextField = new TextField("Value");
        valueTextField.setValue(value);
        valueTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        add(keyTextField, new Text(delimiter), valueTextField, clearButtonBadge);
    }


    public String getKey() {
        return keyTextField.getValue();
    }

    public String getValue() {
        return valueTextField.getValue();
    }

    public void removeFromParent() {
        getElement().removeFromParent();
    }

    public void addClearButtonClickListener(ComponentEventListener<ClickEvent<Button>> eventListener) {
        clearButton.addClickListener(eventListener);
    }

    public boolean hasValue() {
        return StringUtils.hasText(getKey()) && StringUtils.hasText(getValue());
    }
}
