package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerNetwork;
import com.matevskial.dockeradmin.api.docker.DockerNetworkService;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkDriver;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;
import com.matevskial.dockeradmin.vaadinfrontend.ButtonFactory;
import com.matevskial.dockeradmin.vaadinfrontend.ComboBoxWithClearButton;
import com.matevskial.dockeradmin.vaadinfrontend.datafields.DataFieldsLayout;
import com.matevskial.dockeradmin.vaadinfrontend.dialog.ActionDialogBuilder;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;

import java.util.Objects;

public class DockerNetworksListView extends VerticalLayout {

    private final DockerNetworkService dockerNetworkService;
    private Grid<DockerNetwork> dockerNetworksGrid;
    private HorizontalLayout toolbar;
    private TextField networkNameTextField;
    private ComboBoxWithClearButton<DockerNetworkDriver> networkDriverComboBox;
    private Checkbox isEnableIpv6CheckBox;
    private Button refreshDockerNetworksButton;

    public DockerNetworksListView(DockerNetworkService dockerNetworkService) {
        this.dockerNetworkService = dockerNetworkService;
        configureDockerNetworksGrid();
        configureToolbar();
        setSizeFull();
        add(toolbar, dockerNetworksGrid);
    }

    private void configureToolbar() {
        configureGetDockerNetworksButton();

        networkNameTextField = new TextField("Name");
        networkNameTextField.setClearButtonVisible(true);

        networkDriverComboBox = new ComboBoxWithClearButton<>("Driver");
        networkDriverComboBox.setItems(DockerNetworkDriver.values());
        networkDriverComboBox.setItemLabelGenerator(item -> item.name().toLowerCase());

        isEnableIpv6CheckBox = new Checkbox("IPv6");
        toolbar = new HorizontalLayout(networkNameTextField, networkDriverComboBox, isEnableIpv6CheckBox, refreshDockerNetworksButton);
        toolbar.setPadding(false);
        toolbar.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
    }

    private void configureDockerNetworksGrid() {
        dockerNetworksGrid = new Grid<>(DockerNetwork.class, false);
        dockerNetworksGrid.setSizeFull();
        dockerNetworksGrid.setAllRowsVisible(true);
        dockerNetworksGrid.setItemDetailsRenderer(createDockerNetworkItemDetailsRenderer());
        dockerNetworksGrid.addColumn(DockerNetwork::getName).setHeader("Name");
        dockerNetworksGrid.addColumn(DockerNetwork::getDriver).setHeader("Driver");
        dockerNetworksGrid.addColumn(new ComponentRenderer<>(dockerNetwork -> {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setPadding(false);
            Span ipv4 = new Span("IPv4");
            ipv4.getElement().getThemeList().add("badge contrast pill");
            layout.add(ipv4);
            if(dockerNetwork.isEnableIpv6()) {
                Span ipv6 = new Span("IPv6");
                ipv6.getElement().getThemeList().add("badge pill");
                layout.add(ipv6);
            }
            return layout;
        })).setHeader("Enabled IP versions");
        dockerNetworksGrid.addColumn(new ComponentRenderer<>(dockerNetwork -> ButtonFactory.createCopyTextButton("Copy inspect command", String.format("docker inspect %s", dockerNetwork.getName()))));
        dockerNetworksGrid.addColumn(new ComponentRenderer<>(dockerNetwork -> {
            Button deleteButton = new Button();
            deleteButton.setIcon(VaadinIcon.TRASH.create());
            deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
            deleteButton.addClickListener(event -> {
                new ActionDialogBuilder()
                        .withTitle(String.format("Delete network \"%s\"?", dockerNetwork.getName()))
                        .withConfirmButtonText("Delete")
                        .withConfirmButtonTheme("error primary")
                        .withConfirmButtonAction(((dialog, buttonClickEvent) -> {
                            doDelete(dockerNetwork);
                            dialog.close();
                            return null;
                        }))
                        .withCancelButtonText("Cancel")
                        .build().open();
            });
            return deleteButton;
        }));

    }

    private void doDelete(DockerNetwork dockerNetwork) {
        String errorMessage = dockerNetworkService.deleteNetwork(dockerNetwork.getId());
        if(errorMessage != null) {
            new ActionDialogBuilder()
                    .withTitle("Error deleting network")
                    .withDescription(errorMessage)
                    .withCancelButtonText("Close")
                    .build().open();
        } else {
            setItems();
        }
    }

    private Renderer<DockerNetwork> createDockerNetworkItemDetailsRenderer() {
        return new ComponentRenderer<>(DataFieldsLayout::new, ((dataFieldsLayout, dockerNetwork) -> {
            dataFieldsLayout.addDataField("ID", dockerNetwork.getId());
            dataFieldsLayout.addDataField("Scope", dockerNetwork.getScope());
            dataFieldsLayout.addDataField("Subnet", dockerNetwork.getSubnet());
            dataFieldsLayout.addDataField("Gateway", dockerNetwork.getGateway());
            dataFieldsLayout.addListDataField("Driver options", dockerNetwork.getDriverOptionsAsList());
        }));
    }

    private void configureGetDockerNetworksButton() {
        refreshDockerNetworksButton = new Button("Refresh");
        refreshDockerNetworksButton.setIcon(VaadinIcon.REFRESH.create());
        refreshDockerNetworksButton.addClickListener(event -> setItems());
        refreshDockerNetworksButton.addClickShortcut(Key.ENTER);
    }

    private DockerNetworkFilter getFilter() {
        return DockerNetworkFilter.builder()
                .name(networkNameTextField.getValue())
                .driver(networkDriverComboBox.getValue())
                .isEnableIpv6(isEnableIpv6CheckBox.getOptionalValue().orElse(false))
                .build();
    }

    public void setItems() {
        dockerNetworksGrid.setItems(dockerNetworkService.getNetworks(getFilter()));
    }

    public void setItemsAndFields(DockerNetworkFilter filter) {
        networkNameTextField.setValue(Objects.requireNonNullElse(filter.getName(), ""));
        networkDriverComboBox.setValue(filter.getDriver());
        isEnableIpv6CheckBox.setValue(filter.isEnableIpv6());
        dockerNetworksGrid.setItems(dockerNetworkService.getNetworks(filter));
    }
}
