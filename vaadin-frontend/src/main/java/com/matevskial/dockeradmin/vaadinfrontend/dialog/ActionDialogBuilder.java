package com.matevskial.dockeradmin.vaadinfrontend.dialog;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.util.StringUtils;

import java.util.function.BiFunction;

public class ActionDialogBuilder {

    private String title;
    private String description;
    private String confirmButtonText = "Confirm";
    private String secondaryButtonText = "";
    private String confirmButtonTheme = "";
    private String cancelButtonText = "Cancel";
    private BiFunction<Dialog, ClickEvent<Button>, Void> confirmButtonAction;
    private BiFunction<Dialog, ClickEvent<Button>, Void> secondaryButtonAction;
    private BiFunction<Dialog, ClickEvent<Button>, Void> cancelButtonAction;

    boolean addConfirmButton = false;
    boolean addSecondaryButton = false;
    boolean addCancelButton = false;

    public ActionDialogBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public ActionDialogBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ActionDialogBuilder withConfirmButtonText(String confirmButtonText) {
        addConfirmButton = true;
        this.confirmButtonText = confirmButtonText;
        return this;
    }

    public ActionDialogBuilder withConfirmButtonAction(BiFunction<Dialog, ClickEvent<Button>, Void> biFunction) {
        addConfirmButton = true;
        this.confirmButtonAction = biFunction;
        return this;
    }

    public ActionDialogBuilder withConfirmButtonTheme(String theme) {
        this.confirmButtonTheme = theme;
        return this;
    }

    public ActionDialogBuilder withSecondaryButtonText(String secondaryButtonText) {
        addSecondaryButton = true;
        this.secondaryButtonText = secondaryButtonText;
        return this;
    }

    public ActionDialogBuilder withSecondaryButtonAction(BiFunction<Dialog, ClickEvent<Button>, Void> biFunction) {
        addSecondaryButton = true;
        this.secondaryButtonAction = biFunction;
        return this;
    }

    public ActionDialogBuilder withCancelButtonText(String cancelButtonText) {
        this.cancelButtonText = cancelButtonText;
        addCancelButton = true;
        return this;
    }

    public ActionDialogBuilder withCancelButtonAction(BiFunction<Dialog, ClickEvent<Button>, Void> biFunction) {
        addCancelButton = true;
        this.cancelButtonAction = biFunction;
        return this;
    }

    public Dialog build() {
        Dialog dialog = new Dialog();
        dialog.setCloseOnOutsideClick(false);

        H2 headline = new H2(title);
        headline.addClassName("dialog-headline");

        Paragraph descriptionParagraph = null;
        if(StringUtils.hasText(description)) {
            descriptionParagraph = new Paragraph(description);
        }

        Button cancelButton = new Button(cancelButtonText);
        if(cancelButtonAction != null) {
            cancelButton.addClickListener(e -> cancelButtonAction.apply(dialog, e));
        } else {
            cancelButton.addClickListener(e -> dialog.close());
        }

        Button confirmButton = new Button(confirmButtonText);
        confirmButton.getThemeNames().add(confirmButtonTheme);
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        if(confirmButtonAction != null) {
            confirmButton.addClickListener(e -> confirmButtonAction.apply(dialog, e));
        }

        Button secondaryButton = new Button(secondaryButtonText);
        if(secondaryButtonAction != null) {
            secondaryButton.addClickListener(e -> secondaryButtonAction.apply(dialog, e));
        }

        HorizontalLayout buttonsHorizontalLayout = new HorizontalLayout();
        if(addConfirmButton) {
            buttonsHorizontalLayout.add(confirmButton);
        }
        if(addSecondaryButton) {
            buttonsHorizontalLayout.add(secondaryButton);
        }
        if(addCancelButton) {
            buttonsHorizontalLayout.add(cancelButton);
        }
        buttonsHorizontalLayout.setPadding(false);

        VerticalLayout dialogLayout = new VerticalLayout();
        dialogLayout.add(headline);
        if(descriptionParagraph != null) {
            dialogLayout.add(descriptionParagraph);
        }
        dialogLayout.add(buttonsHorizontalLayout);
        dialogLayout.setPadding(false);
        dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);
        dialogLayout.setAlignSelf(FlexComponent.Alignment.END, buttonsHorizontalLayout);

        dialog.add(dialogLayout);
        return dialog;
    }
}
