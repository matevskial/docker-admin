package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.*;
import com.matevskial.dockeradmin.api.docker.filter.DockerContainerFilter;
import com.matevskial.dockeradmin.api.docker.filter.DockerContainerRunningState;
import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;
import com.matevskial.dockeradmin.vaadinfrontend.ComboBoxWithClearButton;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route(value = "containers", layout = DockerAdminLayout.class)
class DockerContainersAdminView extends VerticalLayout {

    private final DockerContainerCreateService dockerContainerCreateService;
    private final DockerContainerService dockerContainerService;
    private final DockerVolumeService dockerVolumeService;
    private final DockerImageService dockerImageService;
    private final DockerNetworkService dockerNetworkService;
    private final DockerContainerLogsService logsService;
    private final DockerContainersAdminViewMapper dockerContainersAdminViewMapper;
    private DockerContainersList dockerContainersList;
    private DockerContainerCreateForm dockerContainerCreateForm;
    private HorizontalLayout toolbar;
    private ComboBox<DockerContainerRunningState> containerStateComboBox;
    private TextField containerNameTextField;
    private ComboBoxWithClearButton<DockerImage> containerImageComboBox;
    private ComboBoxWithClearButton<DockerNetwork> containerNetworkComboBox;
    private Scroller tabContent;
    private Tabs tabs;
    private Tab containersTab;
    private Tab createNewContainerTab;

    public DockerContainersAdminView(DockerContainerCreateService dockerContainerCreateService, DockerContainerService dockerContainerService, DockerVolumeService dockerVolumeService, DockerImageService dockerImageService, DockerNetworkService dockerNetworkService, DockerContainerLogsService logsService, DockerContainersAdminViewMapper dockerContainersAdminViewMapper) {
        this.dockerContainerCreateService = dockerContainerCreateService;
        this.dockerContainerService = dockerContainerService;
        this.dockerVolumeService = dockerVolumeService;
        this.dockerImageService = dockerImageService;
        this.dockerNetworkService = dockerNetworkService;
        this.logsService = logsService;
        this.dockerContainersAdminViewMapper = dockerContainersAdminViewMapper;
        configureTabContent();
        configureDockerContainersList();
        configureDockerContainerCreateForm();
        configureTabs();
        configureToolbar();
        add(tabs);
        add(toolbar);
        add(tabContent);
        populateTabContent(tabs.getSelectedTab());
        dockerContainersList.setContainers(dockerContainerService.getContainers(getFilter()));
    }

    private void configureToolbar() {
        containerStateComboBox = new ComboBox<>("Running state");
        containerStateComboBox.setItems(DockerContainerRunningState.values());
        containerStateComboBox.setValue(DockerContainerRunningState.RUNNING);

        containerNameTextField = new TextField("Name");
        containerNameTextField.setClearButtonVisible(true);

        containerImageComboBox = new ComboBoxWithClearButton<>("Image");
        containerImageComboBox.setItemLabelGenerator(DockerImage::getRepoTag);
        containerImageComboBox.setItems(dockerImageService.getImages(DockerImageFilter.builder().build()));

        containerNetworkComboBox = new ComboBoxWithClearButton<>("Network");
        containerNetworkComboBox.setItemLabelGenerator(DockerNetwork::getName);
        containerNetworkComboBox.setItems(dockerNetworkService.getNetworks(DockerNetworkFilter.builder().build()));

        Button refreshContainersButton = new Button("Refresh");
        refreshContainersButton.setIcon(VaadinIcon.REFRESH.create());
        refreshContainersButton.addClickListener(event -> {
            dockerContainersList.setContainers(dockerContainerService.getContainers(getFilter()));
        });
        refreshContainersButton.addClickShortcut(Key.ENTER);

        toolbar = new HorizontalLayout(containerStateComboBox, containerNameTextField, containerImageComboBox, containerNetworkComboBox, refreshContainersButton);
        toolbar.setPadding(false);
        toolbar.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
    }

    private DockerContainerFilter getFilter() {
        DockerContainerRunningState runningState = containerStateComboBox.getValue() == null ?
                DockerContainerRunningState.RUNNING : containerStateComboBox.getValue();
        return DockerContainerFilter.builder()
                .runningState(runningState)
                .name(containerNameTextField.getValue())
                .imageId(containerImageComboBox.getValue() == null ? null : containerImageComboBox.getValue().getId())
                .networkName(containerNetworkComboBox.getValue() == null ? null : containerNetworkComboBox.getValue().getName())
                .build();
    }

    private void configureDockerContainerCreateForm() {
        dockerContainerCreateForm = new DockerContainerCreateForm(dockerContainerCreateService, dockerVolumeService, dockerImageService, dockerNetworkService);
        dockerContainerCreateForm.addListener(DockerContainerCreateForm.ContainerCreatedEvent.class, event -> {
           tabs.setSelectedTab(containersTab);
           setFilterFields(event.getFilter());
           dockerContainersList.setContainers(dockerContainerService.getContainers(event.getFilter()));
        });
    }

    private void setFilterFields(DockerContainerFilter filter) {
        if(filter.getRunningState() == null) {
            containerStateComboBox.setValue(DockerContainerRunningState.ALL);
        } else {
            containerStateComboBox.setValue(filter.getRunningState());
        }
        containerNameTextField.setValue(filter.getName());
    }

    private void configureTabContent() {
        tabContent = new Scroller();
        tabContent.setWidthFull();
    }

    private void configureDockerContainersList() {
        dockerContainersList = new DockerContainersList(dockerContainerService, logsService);
        dockerContainersList.addListener(DockerContainersList.StartContainerEvent.class, event -> {
            dockerContainersList.setContainers(dockerContainerService.getContainers(getFilter()));
        });
        dockerContainersList.addListener(DockerContainersList.StopContainerEvent.class, event -> {
            dockerContainersList.setContainers(dockerContainerService.getContainers(getFilter()));
        });
        dockerContainersList.addListener(DockerContainersList.DeleteContainerEvent.class, event -> {
            dockerContainersList.setContainers(dockerContainerService.getContainers(getFilter()));
        });
        dockerContainersList.addListener(DockerContainersList.DuplicateContainerEvent.class, event -> {
            DockerContainerCreateFormData formData = dockerContainersAdminViewMapper.toFormData(event.getContainer());
            dockerContainerCreateForm.setContainer(formData);
            tabs.setSelectedTab(createNewContainerTab);
        });
    }

    private void configureTabs() {
        containersTab = new Tab("List containers");
        createNewContainerTab = new Tab("Create new Container");
        tabs = new Tabs(containersTab, createNewContainerTab);
        tabs.addSelectedChangeListener(event -> populateTabContent(event.getSelectedTab()));
    }

    private void populateTabContent(Tab selectedTab) {
        if(containersTab.equals(selectedTab)) {
            tabContent.setContent(dockerContainersList);
            toolbar.setVisible(true);
        } else if(createNewContainerTab.equals(selectedTab)) {
            tabContent.setContent(dockerContainerCreateForm);
            toolbar.setVisible(false);
        }
    }
}
