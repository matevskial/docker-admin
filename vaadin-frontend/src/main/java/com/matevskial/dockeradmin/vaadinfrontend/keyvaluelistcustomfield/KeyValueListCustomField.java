package com.matevskial.dockeradmin.vaadinfrontend.keyvaluelistcustomfield;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KeyValueListCustomField extends CustomField<Map<String, String>> {

    private final List<KeyValueEntry> keyValueEntries = new ArrayList<>();
    private final VerticalLayout layout;
    private final String delimiter;

    public KeyValueListCustomField() {
        this("", "");
    }

    public KeyValueListCustomField(String labelText) {
        this(labelText, "");
    }

    public KeyValueListCustomField(String labelText, String delimiter) {
        this.delimiter = delimiter;
        layout = new VerticalLayout();
        layout.setPadding(false);
        HorizontalLayout labelLayout = new HorizontalLayout();
        labelLayout.setPadding(false);

        Button plusButton = new Button();
        plusButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_TERTIARY_INLINE);
        plusButton.setIcon(VaadinIcon.PLUS.create());
        Span plusButtonBadge = new Span(plusButton);
        plusButtonBadge.getElement().getThemeList().add("badge contrast");
        plusButton.addClickListener(event -> {
            KeyValueEntry keyValueEntry = createKeyValueEntry();
            keyValueEntries.add(keyValueEntry);
            layout.add(keyValueEntry);
        });

        labelLayout.add(new Span(labelText), plusButtonBadge);
        labelLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
        add(labelLayout, layout);
    }

    private KeyValueEntry createKeyValueEntry() {
        KeyValueEntry keyValueEntry = new KeyValueEntry(delimiter);
        keyValueEntry.addClearButtonClickListener(event -> {
            keyValueEntries.remove(keyValueEntry);
            keyValueEntry.removeFromParent();
        });

        return keyValueEntry;
    }

    private KeyValueEntry createKeyValueEntry(String key, String value) {
        KeyValueEntry keyValueEntry = new KeyValueEntry(key, value);
        keyValueEntry.addClearButtonClickListener(event -> {
            keyValueEntries.remove(keyValueEntry);
            keyValueEntry.removeFromParent();
        });

        return keyValueEntry;
    }

    @Override
    public void clear() {
        layout.removeAll();
        keyValueEntries.clear();
    }

    @Override
    public Map<String, String> getValue() {
        if(keyValueEntries == null || keyValueEntries.isEmpty()) {
            return null;
        }
        return generateModelValue();
    }

    @Override
    protected Map<String, String> generateModelValue() {
        Map<String, String> result = new HashMap<>();
        for(KeyValueEntry keyValueEntry : keyValueEntries) {
            if(keyValueEntry.hasValue()) {
                result.put(keyValueEntry.getKey(), keyValueEntry.getValue());
            }
        }
        return result;
    }

    @Override
    protected void setPresentationValue(Map<String, String> value) {
        clear();
        if(value == null) {
            return;
        }
        for(Map.Entry<String, String> entry : value.entrySet()) {
            KeyValueEntry keyValueEntry = createKeyValueEntry(entry.getKey(), entry.getValue());
            layout.add(keyValueEntry);
            keyValueEntries.add(keyValueEntry);
        }
    }
}
