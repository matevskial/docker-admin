package com.matevskial.dockeradmin.vaadinfrontend;

import com.matevskial.dockeradmin.api.UserData;
import com.matevskial.dockeradmin.security.api.SecurityService;
import com.matevskial.dockeradmin.vaadinfrontend.preferences.PreferencesView;
import com.matevskial.dockeradmin.vaadinfrontend.userstatus.UserStatus;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import java.util.Optional;

@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-details-summary-width-maximum.css",themeFor = "vaadin-details")
@JsModule("./js/copytoclipboard/copytoclipboard.js")
public class MainLayout extends AppLayout {

    private final SecurityService securityService;

    public MainLayout(SecurityService securityService) {
        this.securityService = securityService;
        addHeaderToNavbar();
    }

    private void addHeaderToNavbar() {
        Component logo = getLogo();
        HorizontalLayout header = new HorizontalLayout(logo, getUserStatus());
        header.expand(logo);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);
        header.setWidth("100%");
        header.addClassNames("header");
        addToNavbar(header);
    }

    private Component getLogo() {
        H1 logoContainer = new H1(getLogoContent());
        logoContainer.addClassNames("logo-container");
        return logoContainer;
    }

    private Component getLogoContent() {
        Span logoText = new Span("Docker Admin");
        logoText.addClassName("logo-content");
        logoText.addClickListener(e -> this.getUI().ifPresent(ui -> ui.navigate("/")));
        return logoText;
    }

    private UserStatus getUserStatus() {
        Optional<UserData> userDataOptional = securityService.getLoggedUser();
        if(userDataOptional.isEmpty()) {
            throw new IllegalStateException("User not logged in, or user not in database");
        }

        UserStatus userStatus = new UserStatus(userDataOptional.get());
        userStatus.setLogoutMenuItemClickListener(e -> securityService.logout());
        userStatus.setPreferencesMenuItemClickListener(e -> this.getUI().ifPresent(ui -> ui.navigate(PreferencesView.class)));
        return userStatus;
    }
}
