package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerVolumeService;
import com.matevskial.dockeradmin.api.docker.filter.DockerVolumeFilter;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;

import java.util.List;
import java.util.Map;

@Route(value = "volumes", layout = DockerAdminLayout.class)
class DockerVolumesAdminView extends VerticalLayout implements BeforeEnterObserver {

    private final DockerVolumeService dockerVolumeService;
    private Scroller tabContent;
    private DockerVolumesListView dockerVolumesListView;
    private DockerVolumeCreateForm dockerVolumeCreateForm;
    private Tabs tabs;
    private Tab dockerVolumesListViewTab;
    private Tab dockerVolumeCreateFormTab;
    
    public DockerVolumesAdminView(DockerVolumeService dockerVolumeService) {
        this.dockerVolumeService = dockerVolumeService;
        configureTabContent();
        configureTabs();
        configureDockerVolumesView();
        configureDockerVolumeCreateForm();
        add(tabs);
        add(tabContent);
        populateTabContent(tabs.getSelectedTab());
    }

    private void configureTabContent() {
        tabContent = new Scroller();
        tabContent.setWidthFull();
    }

    private void configureTabs() {
        dockerVolumesListViewTab = new Tab("List volumes");
        dockerVolumeCreateFormTab = new Tab("Create volume");
        tabs = new Tabs(dockerVolumesListViewTab, dockerVolumeCreateFormTab);
        tabs.addSelectedChangeListener(event -> populateTabContent(event.getSelectedTab()));
    }

    private void configureDockerVolumesView() {
        dockerVolumesListView = new DockerVolumesListView(dockerVolumeService);
    }

    private void configureDockerVolumeCreateForm() {
        dockerVolumeCreateForm = new DockerVolumeCreateForm(dockerVolumeService);
        dockerVolumeCreateForm.addListener(DockerVolumeCreateForm.VolumeCreatedEvent.class, event -> {
           tabs.setSelectedTab(dockerVolumesListViewTab);
           dockerVolumesListView.setItems();
        });
    }

    private void populateTabContent(Tab selectedTab) {
        if(dockerVolumesListViewTab.equals(selectedTab)) {
            tabContent.setContent(dockerVolumesListView);
        } else if(dockerVolumeCreateFormTab.equals(selectedTab)) {
            tabContent.setContent(dockerVolumeCreateForm);
        }
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters
                .getParameters();
        String volumeName = parametersMap.getOrDefault("name", List.of("")).get(0);
        dockerVolumesListView.setItemsAndFields(DockerVolumeFilter.builder()
                .name(volumeName)
                .build());
    }
}
