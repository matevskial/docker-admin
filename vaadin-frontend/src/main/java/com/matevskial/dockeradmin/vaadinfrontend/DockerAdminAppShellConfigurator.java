package com.matevskial.dockeradmin.vaadinfrontend;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.theme.Theme;

@Theme("docker-admin-theme")
class DockerAdminAppShellConfigurator implements AppShellConfigurator {
}
