package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;

class EnvironmentVariablesCustomField extends CustomField<String> {

    public EnvironmentVariablesCustomField() {
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        HorizontalLayout label = new HorizontalLayout();
        label.setPadding(false);

        Button plusButton = new Button();
        plusButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_TERTIARY_INLINE);
        plusButton.setIcon(VaadinIcon.PLUS.create());
        Span plusButtonBadge = new Span(plusButton);
        plusButtonBadge.getElement().getThemeList().add("badge contrast");
        label.add(new Span("Environment variables"), plusButtonBadge);
        label.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
        layout.add(label);
        layout.add(createEnvironmentVariableEntry());
        add(layout);
    }

    private Component createEnvironmentVariableEntry() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setPadding(false);
        layout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
        Button clearButton = new Button();
        clearButton.setIcon(VaadinIcon.CLOSE_SMALL.create());
        clearButton.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY_INLINE);
        Span clearButtonBadge = new Span(clearButton);
        clearButtonBadge.getElement().getThemeList().add("badge  small");

        TextField nameTextField = new TextField("Name");
        nameTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        TextField valueTextField = new TextField("Value");
        valueTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        layout.add(nameTextField, new Text("="), valueTextField, clearButtonBadge);
        return layout;
    }

    @Override
    protected String generateModelValue() {
        return "e";
    }

    @Override
    protected void setPresentationValue(String s) {

    }
}
