package com.matevskial.dockeradmin.vaadinfrontend;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Div;

public class ComboBoxWithClearButton<T> extends ComboBox<T> {

    private final Div clearDiv;

    public ComboBoxWithClearButton() {
        this("");
    }

    public ComboBoxWithClearButton(String label) {
        super(label);
        clearDiv = new Div(new Text("Clear"));
        clearDiv.getElement().getStyle().set("cursor", "pointer");
        clearDiv.addClickListener(event -> setValue(null));
        setHelperComponent(clearDiv);
    }

    public void addClearClickListener(ComponentEventListener<ClickEvent<Div>> event) {
        clearDiv.addClickListener(event);
    }
}
