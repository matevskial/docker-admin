package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerNetworkService;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;

import java.util.List;
import java.util.Map;

@Route(value = "networks", layout = DockerAdminLayout.class)
class DockerNetworksAdminView extends VerticalLayout implements BeforeEnterObserver {

    private final DockerNetworkService dockerNetworkService;
    private Scroller tabContent;
    private DockerNetworksListView dockerNetworksListView;
    private DockerNetworkCreateForm dockerNetworkCreateForm;
    private Tabs tabs;
    private Tab dockerNetworksListViewTab;
    private Tab dockerNetworkCreateFormTab;

    public DockerNetworksAdminView(DockerNetworkService dockerNetworkService) {
        this.dockerNetworkService = dockerNetworkService;
        configureTabContent();
        configureTabs();
        configureDockerNetworksView();
        configureDockerNetworkCreateForm();
        add(tabs);
        add(tabContent);
        populateTabContent(tabs.getSelectedTab());
    }

    private void configureTabContent() {
        tabContent = new Scroller();
        tabContent.setWidthFull();
    }

    private void configureTabs() {
        dockerNetworksListViewTab = new Tab("List networks");
        dockerNetworkCreateFormTab = new Tab("Create network");
        tabs = new Tabs(dockerNetworksListViewTab, dockerNetworkCreateFormTab);
        tabs.addSelectedChangeListener(event -> populateTabContent(event.getSelectedTab()));
    }

    private void configureDockerNetworksView() {
        dockerNetworksListView = new DockerNetworksListView(dockerNetworkService);
    }

    private void configureDockerNetworkCreateForm() {
        dockerNetworkCreateForm = new DockerNetworkCreateForm(dockerNetworkService);
        dockerNetworkCreateForm.addListener(DockerNetworkCreateForm.NetworkCreatedEvent.class, event -> {
            tabs.setSelectedTab(dockerNetworksListViewTab);
        });
    }

    private void populateTabContent(Tab selectedTab) {
        if(dockerNetworksListViewTab.equals(selectedTab)) {
            tabContent.setContent(dockerNetworksListView);
        } else if(dockerNetworkCreateFormTab.equals(selectedTab)) {
            tabContent.setContent(dockerNetworkCreateForm);
        }
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        Location location = event.getLocation();
        QueryParameters queryParameters = location.getQueryParameters();
        Map<String, List<String>> parametersMap = queryParameters
                .getParameters();
        String networkName = parametersMap.getOrDefault("name", List.of("")).get(0);
        dockerNetworksListView.setItemsAndFields(DockerNetworkFilter.builder()
                .name(networkName)
                .build());
    }
}
