package com.matevskial.dockeradmin.vaadinfrontend;

import com.vaadin.flow.component.textfield.TextArea;

import java.util.List;

public class LogsArea extends TextArea {

    public void setValue(List<String> lines) {
        setValue(getStringValue(lines));
    }

    private String getStringValue(List<String> lines) {
        StringBuilder sb = new StringBuilder();
        lines.forEach(line -> sb.append(line).append('\n'));
        return sb.toString();
    }
}
