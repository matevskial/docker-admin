package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;

class PortMappingsCustomField extends CustomField<String> {

    public PortMappingsCustomField() {
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        HorizontalLayout label = new HorizontalLayout();
        label.setPadding(false);

        Button plusButton = new Button();
        plusButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_TERTIARY_INLINE);
        plusButton.setIcon(VaadinIcon.PLUS.create());
        Span plusButtonBadge = new Span(plusButton);
        plusButtonBadge.getElement().getThemeList().add("badge contrast");
        label.add(new Span("Port mappings"), plusButtonBadge);
        label.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
        layout.add(label);
        layout.add(createPortMappingEntry());
        add(layout);
    }

    private Component createPortMappingEntry() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setPadding(false);
        layout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
        Button clearButton = new Button();
        clearButton.setIcon(VaadinIcon.CLOSE_SMALL.create());
        clearButton.addThemeVariants(ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY_INLINE);
        Span clearButtonBadge = new Span(clearButton);
        clearButtonBadge.getElement().getThemeList().add("badge  small");

        TextField hostPortTextField = new TextField("Host port");
        hostPortTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);

        TextField containerPortTextField = new TextField("Container port");
        containerPortTextField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        layout.add(hostPortTextField, new Text("->"), containerPortTextField, clearButtonBadge);
        return layout;
    }

    @Override
    protected String generateModelValue() {
        return "p";
    }

    @Override
    protected void setPresentationValue(String s) {

    }
}
