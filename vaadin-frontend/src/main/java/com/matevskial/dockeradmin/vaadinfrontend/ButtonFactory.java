package com.matevskial.dockeradmin.vaadinfrontend;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;

public class ButtonFactory {

    public static Button createCopyTextButton(String label, String textToCopy) {
        Button button = new Button();
        if(label != null) {
            button.setText(label);
        }
        button.setIcon(VaadinIcon.COPY.create());
        button.addClickListener(e -> Clipboard.setText(textToCopy));
        return button;
    }

    public static Button createLinkButton(String label, String url) {
        Button button = new Button();
        if(label != null) {
            button.setText(label);
        }
        button.setIcon(VaadinIcon.LINK.create());
        button.addClickListener(e -> e.getSource().getUI().ifPresent(ui -> ui.getPage().open(url, "_blank")));
        return button;
    }
}
