package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerImage;
import com.matevskial.dockeradmin.api.docker.DockerImageService;
import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;
import com.matevskial.dockeradmin.vaadinfrontend.ButtonFactory;
import com.matevskial.dockeradmin.vaadinfrontend.datafields.DataFieldsLayout;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
class DockerImagesListView extends VerticalLayout {

    private final DockerImageService dockerImageService;
    private Grid<DockerImage> dockerImagesGrid;
    private HorizontalLayout toolbar;
    private TextField imageRepoTagTextField;
    private Button refreshImagesButton;

    public DockerImagesListView(DockerImageService dockerImageService) {
        this.dockerImageService = dockerImageService;
        configureDockerImagesGrid();
        configureToolbar();
        setSizeFull();
        add(toolbar, dockerImagesGrid);
    }

    private void configureToolbar() {
        configureGetImagesButton();
        imageRepoTagTextField = new TextField("Repository:Tag");
        imageRepoTagTextField.setClearButtonVisible(true);
        toolbar = new HorizontalLayout(imageRepoTagTextField, refreshImagesButton);
        toolbar.setPadding(false);
        toolbar.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
    }

    private void configureGetImagesButton() {
        refreshImagesButton = new Button("Refresh");
        refreshImagesButton.setIcon(VaadinIcon.REFRESH.create());
        refreshImagesButton.addClickListener(event -> setItems());
        refreshImagesButton.addClickShortcut(Key.ENTER);
    }

    private DockerImageFilter getFilter() {
        return DockerImageFilter.builder().repoTag(imageRepoTagTextField.getValue()).build();
    }

    private void configureDockerImagesGrid() {
        dockerImagesGrid = new Grid<>(DockerImage.class, false);
        dockerImagesGrid.setSizeFull();
        dockerImagesGrid.setAllRowsVisible(true);
        dockerImagesGrid.setItemDetailsRenderer(createDockerImageItemDetailsRenderer());
        dockerImagesGrid.addColumn(DockerImage::getRepoTag).setHeader("Repository:Tag");
        dockerImagesGrid
                .addColumn(dockerImage -> dockerImage.getOperatingSystem() + "/" + dockerImage.getArchitecture())
                .setHeader("Operating system/architecture");
        dockerImagesGrid.addColumn(new ComponentRenderer<>(dockerImage -> ButtonFactory.createCopyTextButton("Copy inspect command", String.format("docker inspect %s", dockerImage.getRepoTag())))).setHeader("Docker inspect command");
        dockerImagesGrid.addColumn(new ComponentRenderer<>(dockerImage -> {
            return ButtonFactory.createLinkButton(null, DockerImage.getDockerHubLink(dockerImage.getRepoTag()));
        })).setHeader("Docker hub").setAutoWidth(true).setFlexGrow(0);
    }

    private Renderer<DockerImage> createDockerImageItemDetailsRenderer() {
        return new ComponentRenderer<>(DataFieldsLayout::new, (dataFieldsLayout, dockerImage) -> {
            dataFieldsLayout.addDataField("ID", dockerImage.getId());
            dataFieldsLayout.addDataField("Created at", dockerImage.getCreatedAt());
            dataFieldsLayout.addListDataField("Command", dockerImage.getCommand());
            dataFieldsLayout.addListDataField("Exposed ports", dockerImage.getExposedPorts());
            dataFieldsLayout.addListDataField("Volumes", dockerImage.getVolumes());
        });
    }

    public void setItems() {
        dockerImagesGrid.setItems(dockerImageService.getImages(getFilter()));
    }

    public void setItemsAndFields(DockerImageFilter filter) {
        imageRepoTagTextField.setValue(Objects.requireNonNullElse(filter.getRepoTag(), ""));
        dockerImagesGrid.setItems(dockerImageService.getImages(filter));
    }
}
