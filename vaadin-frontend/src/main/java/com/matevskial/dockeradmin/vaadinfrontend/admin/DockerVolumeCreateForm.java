package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerVolumeCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerVolumeCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerVolumeService;
import com.matevskial.dockeradmin.vaadinfrontend.dialog.ActionDialogBuilder;
import com.matevskial.dockeradmin.vaadinfrontend.keyvaluelistcustomfield.KeyValueListCustomField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

public class DockerVolumeCreateForm extends FormLayout {

    private final DockerVolumeService dockerVolumeService;
    private final TextField name = new TextField("Name");
    private final TextField driver = new TextField("Driver");
    private final KeyValueListCustomField driverOptions = new KeyValueListCustomField("Driver options", "=");
    private final Button createButton = new Button("Create");
    private final Button resetButton = new Button("Reset");

    private final Binder<DockerVolumeCreateFormData> binder = new BeanValidationBinder<>(DockerVolumeCreateFormData.class);

    public DockerVolumeCreateForm(DockerVolumeService dockerVolumeService) {
        this.dockerVolumeService = dockerVolumeService;

        binder.bindInstanceFields(this);

        setResponsiveSteps(new ResponsiveStep("0", 1));
        driver.setReadOnly(true);
        driver.setValue("local");
        add(name, driver, driverOptions,  createButtonsLayout());
    }

    private Component createButtonsLayout() {
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        createButton.addClickListener(event -> {
            DockerVolumeCreateFormData formData = new DockerVolumeCreateFormData();
            try {
                binder.writeBean(formData);
                showDialog(dockerVolumeService.createVolume(formData));
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        });

        resetButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        resetButton.addClickListener(event -> {
            Dialog dialog = new ActionDialogBuilder()
                    .withTitle("Reset all fields?")
                    .withConfirmButtonText("Reset")
                    .withCancelButtonText("Cancel")
                    .withConfirmButtonAction((dialog1, event1) -> {
                        resetFields();
                        dialog1.close();
                        return null;
                    })
                    .build();
            dialog.open();
        });
        return new HorizontalLayout(createButton, resetButton);
    }

    private void resetFields() {
        name.clear();
        driverOptions.clear();
    }

    private void showDialog(DockerVolumeCreateResponse volumeCreateResponse) {
        Dialog dialog;
        if(volumeCreateResponse.isError()) {
            dialog = new ActionDialogBuilder()
                    .withTitle("Error creating volume")
                    .withDescription(volumeCreateResponse.getErrorMessage())
                    .withCancelButtonText("Return to form")
                    .build();
        } else {
            dialog = new ActionDialogBuilder()
                    .withTitle("Volume created")
                    .withDescription(String.format("Created volume mountpoint: %s", volumeCreateResponse.getMountPoint()))
                    .withConfirmButtonText("Return to volumes list")
                    .withConfirmButtonAction((dialog1, event) -> {
                        fireEvent(new VolumeCreatedEvent(this));
                        dialog1.close();
                        return null;
                    })
                    .withCancelButtonText("Return to form")
                    .build();
        }
        dialog.open();
    }

    public static abstract class DockerVolumeCreateFormEvent extends ComponentEvent<DockerVolumeCreateForm> {

        protected DockerVolumeCreateFormEvent(DockerVolumeCreateForm source) {
            super(source, false);
        }
    }

    public static class VolumeCreatedEvent extends DockerVolumeCreateFormEvent {

        protected VolumeCreatedEvent(DockerVolumeCreateForm source) {
            super(source);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
