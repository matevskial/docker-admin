package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerVolume;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VolumesCustomField extends CustomField<Map<String, String>> {

    private final List<VolumeEntry> volumeEntries = new ArrayList<>();
    @Setter
    private List<DockerVolume> volumes;
    private List<String> pathsInContainer = List.of();
    private final VerticalLayout layout;

    public VolumesCustomField() {
        layout = new VerticalLayout();
        layout.setPadding(false);
        HorizontalLayout labelLayout = new HorizontalLayout();
        labelLayout.setPadding(false);

        Button plusButton = new Button();
        plusButton.addThemeVariants(ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_TERTIARY_INLINE);
        plusButton.setIcon(VaadinIcon.PLUS.create());
        Span plusButtonBadge = new Span(plusButton);
        plusButtonBadge.getElement().getThemeList().add("badge contrast");
        plusButton.addClickListener(event -> {
            VolumeEntry keyValueEntry = createVolumeEntry();
            volumeEntries.add(keyValueEntry);
            layout.add(keyValueEntry);
        });

        labelLayout.add(new Span("Volumes"), plusButtonBadge);
        labelLayout.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.BASELINE);
        layout.add(labelLayout);
        add(layout);
    }

    private VolumeEntry createVolumeEntry() {
        VolumeEntry keyValueEntry = new VolumeEntry(volumes, pathsInContainer);
        keyValueEntry.addClearButtonClickListener(event -> {
            volumeEntries.remove(keyValueEntry);
            keyValueEntry.removeFromParent();
        });

        return keyValueEntry;
    }

    private VolumeEntry createVolumeEntry(String key, String value) {
        VolumeEntry keyValueEntry = new VolumeEntry(volumes, pathsInContainer, key, value);
        keyValueEntry.addClearButtonClickListener(event -> {
            volumeEntries.remove(keyValueEntry);
            keyValueEntry.removeFromParent();
        });

        return keyValueEntry;
    }

    public void setPathsInContainer(List<String> pathsInContainer) {
        this.pathsInContainer = pathsInContainer;
        for(VolumeEntry entry : volumeEntries) {
            entry.setPathsInContainer(pathsInContainer);
        }
    }

    @Override
    public void clear() {
        pathsInContainer = List.of();
        for(VolumeEntry keyValueEntry : volumeEntries) {
            keyValueEntry.removeFromParent();
        }
        volumeEntries.clear();
    }

    @Override
    public Map<String, String> getValue() {
        if(volumeEntries == null || volumeEntries.isEmpty()) {
            return null;
        }
        return generateModelValue();
    }

    @Override
    protected Map<String, String> generateModelValue() {
        Map<String, String> result = new HashMap<>();
        for(VolumeEntry keyValueEntry : volumeEntries) {
            if(keyValueEntry.hasValue()) {
                result.put(keyValueEntry.getVolume(), keyValueEntry.getPathInContainer());
            }
        }
        return result;
    }

    @Override
    protected void setPresentationValue(Map<String, String> value) {
        clear();
        if(value == null) {
            return;
        }
        for(Map.Entry<String, String> entry : value.entrySet()) {
            VolumeEntry volumeEntry = createVolumeEntry(entry.getKey(), entry.getValue());
            layout.add(volumeEntry);
            volumeEntries.add(volumeEntry);
        }
    }
}
