package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.DockerVolume;
import com.matevskial.dockeradmin.api.docker.DockerVolumeService;
import com.matevskial.dockeradmin.api.docker.filter.DockerVolumeFilter;
import com.matevskial.dockeradmin.vaadinfrontend.ButtonFactory;
import com.matevskial.dockeradmin.vaadinfrontend.datafields.DataFieldsLayout;
import com.matevskial.dockeradmin.vaadinfrontend.dialog.ActionDialogBuilder;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;

import java.util.Objects;

public class DockerVolumesListView extends VerticalLayout {

    private final DockerVolumeService dockerVolumeService;
    private Grid<DockerVolume> dockerVolumesGrid;
    private HorizontalLayout toolbar;
    private TextField volumeNameTextField;
    private Button refreshDockerVolumesButton;

    public DockerVolumesListView(DockerVolumeService dockerVolumeService) {
        this.dockerVolumeService = dockerVolumeService;
        configureToolbar();
        configureDockerVolumesGrid();
        setSizeFull();
        add(toolbar, dockerVolumesGrid);
    }

    private void configureToolbar() {
        configureGetDockerVolumesButton();
        volumeNameTextField = new TextField("Name");
        volumeNameTextField.setClearButtonVisible(true);

        toolbar = new HorizontalLayout(volumeNameTextField, refreshDockerVolumesButton);
        toolbar.setPadding(false);
        toolbar.setDefaultVerticalComponentAlignment(Alignment.BASELINE);
    }

    private void configureDockerVolumesGrid() {
        dockerVolumesGrid = new Grid<>(DockerVolume.class, false);
        dockerVolumesGrid.setSizeFull();
        dockerVolumesGrid.setAllRowsVisible(true);
        dockerVolumesGrid.setItemDetailsRenderer(createDockerVolumeItemDetailsRenderer());
        dockerVolumesGrid.addColumn(DockerVolume::getName).setHeader("Name");
        dockerVolumesGrid.addColumn(DockerVolume::getDriver).setHeader("Driver");
        dockerVolumesGrid.addColumn(new ComponentRenderer<>(dockerVolume -> ButtonFactory.createCopyTextButton("Copy inspect command", String.format("docker inspect %s", dockerVolume.getName()))));
        dockerVolumesGrid.addColumn(new ComponentRenderer<>(dockerVolume -> {
            Button deleteButton = new Button();
            deleteButton.setIcon(VaadinIcon.TRASH.create());
            deleteButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
            deleteButton.addClickListener(event -> {
                new ActionDialogBuilder()
                        .withTitle(String.format("Delete volume \"%s\"?", dockerVolume.getName()))
                        .withConfirmButtonText("Delete")
                        .withConfirmButtonTheme("error primary")
                        .withConfirmButtonAction(((dialog, buttonClickEvent) -> {
                            doDelete(dockerVolume);
                            dialog.close();
                            return null;
                        }))
                        .withCancelButtonText("Cancel")
                        .build().open();
            });
            return deleteButton;
        }));

    }

    private void doDelete(DockerVolume dockerVolume) {
        String errorMessage = dockerVolumeService.deleteVolume(dockerVolume.getName());
        if(errorMessage != null) {
            new ActionDialogBuilder()
                    .withTitle("Error deleting volume")
                    .withDescription(errorMessage)
                    .withCancelButtonText("Close")
                    .build().open();
        } else {
            setItems();
        }
    }

    private void configureGetDockerVolumesButton() {
        refreshDockerVolumesButton = new Button("Refresh");
        refreshDockerVolumesButton.setIcon(VaadinIcon.REFRESH.create());
        refreshDockerVolumesButton.addClickListener(event -> setItems());
        refreshDockerVolumesButton.addClickShortcut(Key.ENTER);
    }

    private DockerVolumeFilter getFilter() {
        return DockerVolumeFilter.builder().name(volumeNameTextField.getValue()).build();
    }

    private Renderer<DockerVolume> createDockerVolumeItemDetailsRenderer() {
        return new ComponentRenderer<>(DataFieldsLayout::new, ((dataFieldsLayout, dockerVolume) -> {
            dataFieldsLayout.addDataField("Mount point", dockerVolume.getMountPoint());
            dataFieldsLayout.addListDataField("Driver options", dockerVolume.getDriverOptionsAsList());
        }));
    }

    public void setItems() {
        dockerVolumesGrid.setItems(dockerVolumeService.getVolumes(getFilter()));
    }

    public void setItemsAndFields(DockerVolumeFilter filter) {
        volumeNameTextField.setValue(Objects.requireNonNullElse(filter.getName(), ""));
        dockerVolumesGrid.setItems(dockerVolumeService.getVolumes(filter));
    }
}
