package com.matevskial.dockeradmin.vaadinfrontend.preferences;

import com.matevskial.dockeradmin.vaadinfrontend.MainLayout;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "/preferences", layout = MainLayout.class)
@PageTitle("Preferences | Docker Admin")
public class PreferencesView extends VerticalLayout {

    public PreferencesView() {
        H1 title = new H1("Preferences");
        add(title);
    }
}
