package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.*;
import com.matevskial.dockeradmin.api.docker.filter.DockerContainerFilter;
import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;
import com.matevskial.dockeradmin.api.docker.filter.DockerVolumeFilter;
import com.matevskial.dockeradmin.vaadinfrontend.dialog.ActionDialogBuilder;
import com.matevskial.dockeradmin.vaadinfrontend.keyvaluelistcustomfield.KeyValueListCustomField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;

import java.util.Objects;
import java.util.stream.Collectors;

class DockerContainerCreateForm extends FormLayout {

    private final DockerContainerCreateService dockerContainerCreateService;
    private final DockerVolumeService dockerVolumeService;
    private final DockerImageService dockerImageService;
    private final DockerNetworkService dockerNetworkService;
    private final TextField name = new TextField("Name");
    private final ComboBox<String> image = new ComboBox<>("Image");
    private final KeyValueListCustomField portMappings = new KeyValueListCustomField("Port mappings", "->");
    private final ComboBox<String> network = new ComboBox<>("Network");
    private final VolumesCustomField volumes = new VolumesCustomField();
    private final KeyValueListCustomField environmentVariables = new KeyValueListCustomField("Environment variables", "=");
    private final Button createButton = new Button("Create");
    private final Button replaceButton = new Button("Replace");
    private final Button resetButton = new Button("Reset");
    private final Checkbox replaceContainerCheckBox = new Checkbox("Replace container");
    private final Binder<DockerContainerCreateFormData> binder = new BeanValidationBinder<>(DockerContainerCreateFormData.class);

    private String containerId;

    public DockerContainerCreateForm(DockerContainerCreateService dockerContainerCreateService, DockerVolumeService dockerVolumeService, DockerImageService dockerImageService, DockerNetworkService dockerNetworkService) {
        binder.bindInstanceFields(this);
        this.dockerContainerCreateService = dockerContainerCreateService;
        this.dockerVolumeService = dockerVolumeService;
        this.dockerImageService = dockerImageService;
        this.dockerNetworkService = dockerNetworkService;
        setResponsiveSteps(new ResponsiveStep("0", 1));
        configureImageComboBox();
        configureNetworkComboBox();
        configureVolumesCustomField();
        configureReplaceContainerCheckBox();
        add(name, image, portMappings, network, volumes, environmentVariables, replaceContainerCheckBox, createButtonsLayout());
    }

    private void configureReplaceContainerCheckBox() {
        replaceContainerCheckBox.addValueChangeListener(event -> {
            if(Objects.requireNonNullElse(event.getValue(), false)) {
                replaceButton.setVisible(true);
                createButton.setVisible(false);
            } else {
                createButton.setVisible(true);
                replaceButton.setVisible(false);
            }
        });
    }

    public void setContainer(DockerContainerCreateFormData formData) {
        resetFields();
        updateComboboxData();
        containerId = formData.getContainerId();
        binder.readBean(formData);
    }

    private void updateComboboxData() {
        network.setItems(dockerNetworkService.getNetworks(DockerNetworkFilter.builder().build())
                .stream().map(DockerNetwork::getName).collect(Collectors.toList()));
        image.setItems(dockerImageService.getImages(DockerImageFilter.builder().build())
                .stream().map(DockerImage::getRepoTag).collect(Collectors.toList()));
        volumes.setVolumes(dockerVolumeService.getVolumes(DockerVolumeFilter.builder().build()));
        setVolumePathsInContainer(image.getValue());
    }

    private void configureNetworkComboBox() {
        network.setItems(dockerNetworkService.getNetworks(DockerNetworkFilter.builder().build())
                .stream().map(DockerNetwork::getName).collect(Collectors.toList()));
    }

    private void configureImageComboBox() {
        image.setItems(dockerImageService.getImages(DockerImageFilter.builder().build())
                .stream().map(DockerImage::getRepoTag).collect(Collectors.toList()));
        image.addValueChangeListener(event -> setVolumePathsInContainer(event.getValue()));
    }

    private void configureVolumesCustomField() {
        volumes.setVolumes(dockerVolumeService.getVolumes(DockerVolumeFilter.builder().build()));
        setVolumePathsInContainer(image.getValue());
    }

    private void setVolumePathsInContainer(String imageRepoTag) {
        if(imageRepoTag != null) {
            dockerImageService
                    .getImages(DockerImageFilter.builder().repoTag(imageRepoTag).build()).stream()
                    .findFirst().ifPresent(image ->  volumes.setPathsInContainer(image.getVolumes()));
        }
    }

    private Component createButtonsLayout() {
        createButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        createButton.addClickListener(event -> {
            DockerContainerCreateFormData formData = new DockerContainerCreateFormData();
            try {
                binder.writeBean(formData);
                showDialog(dockerContainerCreateService.createContainer(formData));
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        });

        replaceButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        replaceButton.setVisible(false);
        replaceButton.addThemeVariants(ButtonVariant.LUMO_ERROR);
        replaceButton.addClickListener(event -> {
            DockerContainerCreateFormData formData = new DockerContainerCreateFormData();
            try {
                formData.setContainerId(containerId);
                binder.writeBean(formData);
                showDialog(dockerContainerCreateService.replaceContainer(formData));
            } catch (ValidationException e) {
                e.printStackTrace();
            }
        });

        resetButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
        resetButton.addClickListener(event -> {
            Dialog dialog = new ActionDialogBuilder()
                    .withTitle("Reset all fields?")
                    .withConfirmButtonText("Reset")
                    .withCancelButtonText("Cancel")
                    .withConfirmButtonAction((dialog1, event1) -> {
                        resetFields();
                        dialog1.close();
                        return null;
                    })
                    .build();
            dialog.open();
        });

        return new HorizontalLayout(createButton, replaceButton, resetButton);
    }

    private void showDialog(DockerContainerCreateResponse containerCreateResponse) {
        Dialog dialog;
        if(containerCreateResponse.isError()) {
            dialog = new ActionDialogBuilder()
                    .withTitle("Error creating container")
                    .withDescription(containerCreateResponse.getErrorMessage())
                    .withCancelButtonText("Return to form")
                    .build();
        } else {
            dialog = new ActionDialogBuilder()
                    .withTitle("Container created")
                    .withDescription(String.format("Id of created container: %s", containerCreateResponse.getContainerId()))
                    .withConfirmButtonText("Return to containers list")
                    .withSecondaryButtonText("View created container")
                    .withSecondaryButtonAction((dialog1, event) -> {
                        fireEvent(new ContainerCreatedEvent(this, DockerContainerFilter
                                .builder().containerId(containerCreateResponse.getContainerId()).name(name.getValue()).build()));
                        dialog1.close();
                        return null;
                    })
                    .withConfirmButtonAction((dialog1, event) -> {
                        fireEvent(new ContainerCreatedEvent(this));
                        dialog1.close();
                        return null;
                    })
                    .withCancelButtonText("Return to form")
                    .build();
        }
        dialog.open();
    }

    private void resetFields() {
        name.clear();
        image.clear();
        portMappings.clear();
        network.clear();
        volumes.clear();
        environmentVariables.clear();
    }

    public static abstract class DockerContainerCreateFormEvent extends ComponentEvent<DockerContainerCreateForm> {

        protected DockerContainerCreateFormEvent(DockerContainerCreateForm source) {
            super(source, false);
        }
    }

    public static class ContainerCreatedEvent extends DockerContainerCreateFormEvent {
        @Getter
        private final DockerContainerFilter filter;

        protected ContainerCreatedEvent(DockerContainerCreateForm source) {
            super(source);
            filter = DockerContainerFilter.builder().build();
        }

        protected ContainerCreatedEvent(DockerContainerCreateForm source, DockerContainerFilter filter) {
            super(source);
            this.filter = filter;
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
