package com.matevskial.dockeradmin.vaadinfrontend.datafields;


import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

import java.util.List;

public class DataFieldsLayout extends VerticalLayout {

    public DataFieldsLayout() {
        setPadding(false);
        setSpacing(false);
    }

    public void addDataField(String fieldName, String fieldValue) {
        TextField field = new TextField();
        field.setValue(fieldValue == null ? "" : fieldValue);
        field.setReadOnly(true);
        field.setWidthFull();
        FormLayout formLayout = new FormLayout();
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));
        formLayout.addFormItem(field, fieldName);
        add(formLayout);
    }

    public void addListDataField(String fieldName, List<String> fieldValues) {
        FormLayout formLayout = new FormLayout();
        formLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.addClassName("list-data-field");
        verticalLayout.setPadding(false);
        verticalLayout.setSpacing(false);
        fieldValues.forEach(fieldValue -> verticalLayout.add(new Span(fieldValue)));
        formLayout.addFormItem(verticalLayout, fieldName);
        add(formLayout);
    }
}
