package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.api.docker.*;
import com.matevskial.dockeradmin.vaadinfrontend.Clipboard;
import com.matevskial.dockeradmin.vaadinfrontend.HintText;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;

class DockerContainerDetails extends Composite<Details> {

    private final DockerContainer container;
    private final DockerContainerLogsService logsService;
    private VerticalLayout tabContent;
    private Tabs tabs;
    private Tab portsTab;
    private Tab volumesTab;
    private Tab networksTab;
    private Tab environmentVariablesTab;
    private Tab logsTab;
    private DockerContainerLogsViewer logsViewer;

    public DockerContainerDetails(DockerContainer container, DockerContainerLogsService logsService) {
        this.container = container;
        this.logsService = logsService;
        configureTabContent();
        configureTabs();
        configureLogsViewer(container);
        VerticalLayout detailsLayout = createLayout();
        detailsLayout.add(tabs, tabContent);
        Details details = getContent();
        details.getStyle().set("margin-top", "1.25em");
        details.setSummaryText("Details");
        details.setContent(detailsLayout);
        populateTabContent(tabs.getSelectedTab());
    }

    private void configureLogsViewer(DockerContainer container) {
        logsViewer = new DockerContainerLogsViewer(logsService, container);
        logsViewer.setPadding(false);
    }

    private void configureTabContent() {
        tabContent = new VerticalLayout();
        tabContent.setSpacing(false);
        tabContent.setPadding(false);
        tabContent.getStyle().set("margin-left", "5%");
    }

    private void configureTabs() {
        tabs = new Tabs();
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.addSelectedChangeListener(event -> populateTabContent(event.getSelectedTab()));
        portsTab = new Tab(VaadinIcon.PLUG.create(), new Span("Ports"));
        volumesTab = new Tab(VaadinIcon.HARDDRIVE.create(), new Span("Volumes"));
        networksTab = new Tab(VaadinIcon.SITEMAP.create(), new Span("Networks"));
        environmentVariablesTab = new Tab(VaadinIcon.CODE.create(), new Span("Environment variables"));
        logsTab = new Tab(VaadinIcon.NOTEBOOK.create(), new Span("Logs"));
        tabs.add(portsTab, volumesTab, networksTab, environmentVariablesTab, logsTab);
    }

    private void populateTabContent(Tab selectedTab) {
        tabContent.removeAll();
        if(portsTab.equals(selectedTab)) {
            tabContent.add(createPortsTabContent());
        } else if(volumesTab.equals(selectedTab)) {
            tabContent.add(createVolumesTabContent());
        } else if(networksTab.equals(selectedTab)) {
            tabContent.add(createNetworksTabContent());
        } else if(environmentVariablesTab.equals(selectedTab)) {
            tabContent.add(createEnvironmentVariablesTabContent());
        } else if(logsTab.equals(selectedTab)) {
            tabContent.add(logsViewer);
        }
    }

    private Component createPortsTabContent() {
        if(container.getPorts().isEmpty()) {
            return new HintText("No host ports mapped to this container's ports");
        } else {
            VerticalLayout layout = new VerticalLayout();
            layout.setPadding(false);
            container.getPorts().forEach(port -> layout.add(createPortContent(port)));
            return layout;
        }
    }

    private Component createPortContent(DockerContainerPort port) {
        Span hostPort = new Span(String.format("%s:%s", port.getHostIp(), port.getHostPort()));
        hostPort.getElement().getThemeList().add("badge contrast");
        hostPort.getElement().setAttribute("title", "Mapped host port");

        Span delimiter = new Span("->");
        delimiter.getElement().getThemeList().add("badge contrast");

        Span containerPort = new Span(port.getContainerPort());
        containerPort.getElement().getThemeList().add("badge contrast");
        containerPort.getElement().setAttribute("title", "Mapped container port");

        Span protocol = new Span(port.getProtocol());
        protocol.getElement().getThemeList().add("badge contrast");
        protocol.getElement().setAttribute("title", "Protocol");

        Span portContent = new Span(hostPort, delimiter, containerPort, protocol);
        portContent.getElement().getThemeList().add("badge contrast");

        return portContent;
    }

    private Component createVolumesTabContent() {
        if(container.getVolumes().isEmpty()) {
            return new HintText("No volumes used by this container");
        } else {
            Grid<DockerContainerVolume> volumesGrid = new Grid<>(DockerContainerVolume.class, false);
            volumesGrid.addColumn(DockerContainerVolume::getName).setHeader("Name").setAutoWidth(true).setFlexGrow(0);
            volumesGrid.addColumn(DockerContainerVolume::getType).setHeader("Type").setAutoWidth(true).setFlexGrow(0);
            volumesGrid.addColumn(new ComponentRenderer<>(dockerContainerVolume -> {
                Button button = new Button();
                button.setIcon(VaadinIcon.LINK.create());
                String location = RouteConfiguration.forSessionScope().getUrl(DockerVolumesAdminView.class);
                button.addClickListener(e -> getUI().ifPresent(
                        ui -> ui.navigate(location, QueryParameters.fromString(String.format("name=%s", dockerContainerVolume.getName())))));
                if(!dockerContainerVolume.getType().equals("volume")) {
                    button.setEnabled(false);
                }
                return button;
            }));
            volumesGrid.setItemDetailsRenderer(createVolumeDetailsRenderer());
            volumesGrid.setAllRowsVisible(true);
            volumesGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
            volumesGrid.setItems(container.getVolumes());
            volumesGrid.setWidthFull();
            return volumesGrid;
        }
    }

    private Renderer<DockerContainerVolume> createVolumeDetailsRenderer() {
        return new ComponentRenderer<>(FormLayout::new, (volumeDetailsFormLayout, dockerContainerVolume) -> {
            volumeDetailsFormLayout.add(createVolumePathField("Mount point", dockerContainerVolume.getSource()));
            volumeDetailsFormLayout.add(createVolumePathField("Path in container", dockerContainerVolume.getDestination()));
            volumeDetailsFormLayout.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));
        });
    }

    private Component createVolumePathField(String fieldName, String fieldValue) {
        TextField field = new TextField(fieldName);
        field.setReadOnly(true);
        field.setValue(fieldValue);
        Button button = new Button();
        button.addThemeVariants(ButtonVariant.LUMO_ICON);
        button.setIcon(VaadinIcon.COPY.create());
        button.getElement().setAttribute("title", "Copy path");
        button.addClickListener(e -> Clipboard.setText(fieldValue));
        field.setSuffixComponent(button);
        return field;
    }

    private Component createNetworksTabContent() {
        if(container.getNetworks().isEmpty()) {
            return new HintText("No networks attached to this container");
        } else {
            Grid<DockerContainerNetwork> networksGrid = new Grid<>(DockerContainerNetwork.class, false);
            networksGrid.addColumn(dockerContainerNetwork -> dockerContainerNetwork.getName() + " (" + dockerContainerNetwork.getDriver() + ")").setHeader("Name").setAutoWidth(true).setFlexGrow(0);
            networksGrid.addColumn(new ComponentRenderer<>(dockerContainerNetwork -> {
                Button button = new Button();
                button.setIcon(VaadinIcon.LINK.create());
                String location = RouteConfiguration.forSessionScope().getUrl(DockerNetworksAdminView.class);
                button.addClickListener(e -> getUI().ifPresent(
                        ui -> ui.navigate(location, QueryParameters.fromString(String.format("name=%s", dockerContainerNetwork.getName())))));
                return button;
            }));
            networksGrid.setItemDetailsRenderer(createNetworkDetailsRenderer());
            networksGrid.setAllRowsVisible(true);
            networksGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
            networksGrid.setItems(container.getNetworks());
            networksGrid.setWidthFull();
            return networksGrid;
        }
    }

    private ComponentRenderer<VerticalLayout, DockerContainerNetwork> createNetworkDetailsRenderer() {
        return new ComponentRenderer<>(VerticalLayout::new, (networkDetailsLayout, dockerContainerNetwork) -> {
            networkDetailsLayout.setPadding(false);
            networkDetailsLayout.setSpacing(false);
            networkDetailsLayout.add(new Span(String.format("ID: %s", dockerContainerNetwork.getId())));
            networkDetailsLayout.add(new Span(String.format("Gateway: %s", dockerContainerNetwork.getGateway())));
            networkDetailsLayout.add(new Span(String.format("Container IP address: %s", dockerContainerNetwork.getContainerIpAddress())));
        });
    }

    private Component createEnvironmentVariablesTabContent() {
        if(container.getEnvironmentVariables().isEmpty()) {
            return new HintText("No environment variables for this container");
        } else {
            Grid<EnvironmentVariable> environmentVariablesGrid = new Grid<>(EnvironmentVariable.class, false);
            environmentVariablesGrid.addColumn(EnvironmentVariable::getName).setHeader("Name").setAutoWidth(true).setFlexGrow(0);
            environmentVariablesGrid.addColumn(EnvironmentVariable::getValue).setHeader("Value").setAutoWidth(true).setFlexGrow(0);
            environmentVariablesGrid.addColumn(new ComponentRenderer<>(Button::new, (button, environmentVariable) -> {
                button.addThemeVariants(ButtonVariant.LUMO_ICON);
                button.setIcon(VaadinIcon.COPY.create());
                button.getElement().setAttribute("title", "Copy environment variable");
                button.addClickListener(e -> Clipboard.setText(String.format("%s=%s", environmentVariable.getName(), environmentVariable.getValue())));
            })).setAutoWidth(true).setFlexGrow(0);
            environmentVariablesGrid.setAllRowsVisible(true);
            environmentVariablesGrid.addThemeVariants(GridVariant.LUMO_COMPACT, GridVariant.LUMO_NO_BORDER);
            environmentVariablesGrid.setItems(container.getEnvironmentVariables());
            return environmentVariablesGrid;
        }

    }

    private VerticalLayout createLayout() {
        VerticalLayout layout = new VerticalLayout();
        layout.setPadding(false);
        layout.setSpacing(false);
        return layout;
    }
}
