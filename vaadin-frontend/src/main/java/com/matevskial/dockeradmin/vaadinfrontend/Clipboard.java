package com.matevskial.dockeradmin.vaadinfrontend;

import com.vaadin.flow.component.UI;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Clipboard {

    public static void setText(String text) {
        UI.getCurrent().getPage().executeJs("window.copyToClipboard($0)", text);
    }
}
