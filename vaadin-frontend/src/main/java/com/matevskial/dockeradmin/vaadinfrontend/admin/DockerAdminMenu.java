package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.RouterLink;

class DockerAdminMenu extends VerticalLayout {

    private final Tabs tabs;

    public DockerAdminMenu() {
        tabs = new Tabs();
        setTabsStyle(tabs);
        addTabEntries(tabs);
        add(tabs);
        setWidth("auto");
        setMinWidth("12%");
    }


    private void setTabsStyle(Tabs tabs) {
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_CENTERED);
        tabs.setAutoselect(false);
    }

    private void addTabEntries(Tabs tabs) {
        for(RouterEntry entry : RouterEntry.values()) {
            Tab t = createRouterLinkTab(entry);
            tabs.add(t);
        }
    }

    private Tab createRouterLinkTab(RouterEntry entry) {
        Tab tab = new Tab();
        RouterLink link = new RouterLink();
        link.setHighlightAction(this::setSelectedTabOnHighlight);
        link.add(new Span(entry.viewName));
        link.setRoute(entry.viewTarget);
        link.setTabIndex(-1);
        tab.add(link);
        return tab;
    }

    private void setSelectedTabOnHighlight(RouterLink routerLink, boolean shouldHighLight) {
        if(shouldHighLight) {
            routerLink.getElement().setAttribute("highlight", "");
            routerLink.getParent().map(Tab.class::cast).ifPresent(tabs::setSelectedTab);
        } else {
            routerLink.getElement().removeAttribute("highlight");
        }
    }

    private enum RouterEntry {

        CONTAINERS("Containers", DockerContainersAdminView.class),
        IMAGES("Images", DockerImagesAdminView.class),
        VOLUMES("Volumes", DockerVolumesAdminView.class),
        NETWORKS("Networks", DockerNetworksAdminView.class),
        ENGINE_INFO("Engine info", DockerEngineInfoAdminView.class);

        private final String viewName;
        private final Class<? extends Component> viewTarget;

        RouterEntry(String viewName, Class<? extends Component> viewTarget) {
            this.viewName = viewName;
            this.viewTarget = viewTarget;
        }
    }
}
