package com.matevskial.dockeradmin.vaadinfrontend.admin;

import com.matevskial.dockeradmin.vaadinfrontend.MainLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;

@ParentLayout(MainLayout.class)
@RoutePrefix("/admin")
class DockerAdminLayout extends HorizontalLayout implements RouterLayout {

    private Component content;

    public DockerAdminLayout() {
        add(new DockerAdminMenu());
        setSizeFull();
    }

    @Override
    public void showRouterLayoutContent(HasElement content) {
        Component target = null;
        if (content != null) {
            target = content.getElement().getComponent().orElseThrow(
                    () -> new IllegalArgumentException("DockerAdminLayout content must be a Component"));
        }
        setContent(target);
    }

    private void setContent(Component content) {
        if(content != null) {
            this.content = content;
            getElement().appendChild(content.getElement());
        }
    }

    @Override
    public void removeRouterLayoutContent(HasElement oldContent) {
        RouterLayout.super.removeRouterLayoutContent(oldContent);
        content = null;
    }
}
