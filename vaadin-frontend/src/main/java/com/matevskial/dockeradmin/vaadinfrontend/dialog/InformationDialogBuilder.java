package com.matevskial.dockeradmin.vaadinfrontend.dialog;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class InformationDialogBuilder {

    private String title;
    private String description;

    public InformationDialogBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public InformationDialogBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public Dialog build() {
        Dialog dialog = new Dialog();
        createLayoutForDialog(dialog);
        return dialog;
    }

    private void createLayoutForDialog(Dialog dialog) {
        H2 headline = new H2(title);
        headline.addClassName("dialog-headline");

        Paragraph paragraph = new Paragraph(description);

        Button closeButton = new Button("Close");
        closeButton.addClickListener(e -> dialog.close());

        VerticalLayout dialogLayout = new VerticalLayout(headline, paragraph,
                closeButton);
        dialogLayout.setPadding(false);
        dialogLayout.setAlignItems(FlexComponent.Alignment.STRETCH);
        dialogLayout.setAlignSelf(FlexComponent.Alignment.END, closeButton);
        dialog.add(dialogLayout);
    }
}
