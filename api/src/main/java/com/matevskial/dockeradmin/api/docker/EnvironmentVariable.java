package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class EnvironmentVariable {

    String name;
    String value;

    @Override
    public String toString() {
        return String.format("%s=%s", name, value);
    }
}
