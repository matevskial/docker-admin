package com.matevskial.dockeradmin.api.docker;

public class DockerException extends RuntimeException {

    public DockerException(String message) {
        super(message);
    }
}
