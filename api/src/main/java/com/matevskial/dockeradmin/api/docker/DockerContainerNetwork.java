package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class DockerContainerNetwork {

    @NonNull
    String id;
    @NonNull
    String name;
    String gateway;
    String containerIpAddress;
    String driver;
}
