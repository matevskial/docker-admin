package com.matevskial.dockeradmin.api.docker.filter;

public enum DockerContainerRunningState {

    RUNNING,
    EXITED,
    ALL
}
