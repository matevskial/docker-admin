package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class DockerContainerPort {

    @NonNull
    String containerPort;
    @NonNull
    String hostPort;
    @Builder.Default
    String hostIp = "";
    @NonNull
    String protocol;
}
