package com.matevskial.dockeradmin.api.docker;

import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;

import java.util.List;

public interface DockerNetworkService {

    List<DockerNetwork> getNetworks(DockerNetworkFilter filter);
    DockerNetworkCreateResponse createNetwork(DockerNetworkCreateFormData dockerNetworkCreateFormData);
    String deleteNetwork(String networkId);
}
