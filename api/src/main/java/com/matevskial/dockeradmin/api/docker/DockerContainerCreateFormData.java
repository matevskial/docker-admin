package com.matevskial.dockeradmin.api.docker;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DockerContainerCreateFormData {

    String containerId;
    String name;
    String image;
    Map<String, String> portMappings;
    String network;
    Map<String, String> volumes;
    Map<String, String> environmentVariables;
}
