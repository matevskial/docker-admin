package com.matevskial.dockeradmin.api.docker;

import com.matevskial.dockeradmin.api.docker.filter.DockerContainerFilter;

import java.util.List;

public interface DockerContainerService {

    List<DockerContainer> getAllContainers();
    List<DockerContainer> getRunningContainers();
    List<DockerContainer> getContainers(DockerContainerFilter filter);
    String startContainer(String containerId);
    String stopContainer(String containerId);
    String deleteContainer(String containerId, boolean deleteVolumes);
    String buildDockerRunCommand(String containerId);
}
