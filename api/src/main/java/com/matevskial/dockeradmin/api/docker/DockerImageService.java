package com.matevskial.dockeradmin.api.docker;

import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;

import java.util.List;

public interface DockerImageService {
    List<DockerImage> getImages(DockerImageFilter filter);
    List<DockerImageSearchItem> searchImages(String searchTerm);
}
