package com.matevskial.dockeradmin.api.docker;

import com.matevskial.dockeradmin.api.docker.logs.DockerContainerLogsRequest;

import java.util.List;

public interface DockerContainerLogsService {

    List<String> getLogs(DockerContainerLogsRequest logsRequest);
}
