package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DockerNetworkCreateResponse {

    String networkId;
    boolean isError;
    String errorMessage;
}
