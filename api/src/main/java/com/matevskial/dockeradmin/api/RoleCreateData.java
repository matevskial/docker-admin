package com.matevskial.dockeradmin.api;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

@Data
@Builder
@ToString
public class RoleCreateData {
    @NonNull
    private String name;
}
