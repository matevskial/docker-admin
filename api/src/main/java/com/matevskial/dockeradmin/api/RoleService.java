package com.matevskial.dockeradmin.api;

import java.util.Optional;

public interface RoleService {
    Optional<RoleData> findByName(String roleName);
    RoleData create(RoleCreateData role);
}
