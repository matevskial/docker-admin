package com.matevskial.dockeradmin.api.docker.filter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DockerVolumeFilter {

    String name;
}
