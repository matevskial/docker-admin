package com.matevskial.dockeradmin.api.docker.filter;

public enum DockerNetworkDriver {

    NONE,
    HOST,
    BRIDGE,
    OVERLAY
}
