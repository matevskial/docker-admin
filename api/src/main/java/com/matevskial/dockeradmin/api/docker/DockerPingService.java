package com.matevskial.dockeradmin.api.docker;

public interface DockerPingService {

    void ping() throws DockerException;
}
