package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
@Builder
public class DockerImage {
    @NonNull
    String id;
    @NonNull
    String createdAt;
    @NonNull
    String repoTag;
    @Builder.Default
    List<String> cmd = new ArrayList<>();
    @Builder.Default
    List<String> entryPoint = new ArrayList<>();
    @Builder.Default
    List<String> exposedPorts = new ArrayList<>();
    @Builder.Default
    List<String> volumes = new ArrayList<>();
    @Builder.Default
    List<EnvironmentVariable> defaultEnvironmentVariables = new ArrayList<>();
    String operatingSystem;
    String architecture;

    public static String getDockerHubLink(String repoTag) {
        String prefix;
        if(repoTag.contains("/")) {
            prefix = "https://hub.docker.com/r/";
        } else {
            prefix = "https://hub.docker.com/_/";
        }
        return prefix + repoTag.split(":")[0];
    }

    public List<String> getCommand() {
        return cmd.isEmpty() ? entryPoint : cmd;
    }
}
