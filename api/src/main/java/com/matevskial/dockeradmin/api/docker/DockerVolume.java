package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Value
@Builder
public class DockerVolume {
    @NonNull
    String name;
    @NonNull
    String driver;
    @NonNull
    String mountPoint;
    @Builder.Default
    Map<String, String> driverOptions = new HashMap<>();

    public List<String> getDriverOptionsAsList() {
        List<String> optionsList = new ArrayList<>();
        if(driverOptions != null) {
            driverOptions.forEach((key, value) -> optionsList.add(String.format("%s=%s", key, value)));
        }
        return optionsList;
    }
}
