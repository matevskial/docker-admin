package com.matevskial.dockeradmin.api;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString
@Builder
public class RoleData {
    @NonNull
    private final String id;
    @NonNull
    private final String name;
}
