package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
@Builder
public class DockerInfo {
    String dockerServerVersion;
    String dockerRootDirectory;
    @Builder.Default
    List<String> volumePlugins = new ArrayList<>();
    @Builder.Default
    List<String> networkPlugins = new ArrayList<>();
    int containers;
    int runningContainers;
    int exitedContainers;
    int images;
    String operatingSystem;
    String osType;
    String kernelVersion;
    String hostname;
    String architecture;
    int numberOfCpus;
    long memory;

    public String getMemoryInGigaBytes() {
        long memoryInGigaBytes = memory / 1_000_000_000;
        return memoryInGigaBytes + "GB";
    }
}
