package com.matevskial.dockeradmin.api.docker;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class DockerVolumeCreateFormData {

    private String name;
    private String driver;
    private Map<String, String> driverOptions;
}
