package com.matevskial.dockeradmin.api.docker;

public interface DockerContainerCreateService {
    DockerContainerCreateResponse createContainer(DockerContainerCreateFormData formData);
    DockerContainerCreateResponse replaceContainer(DockerContainerCreateFormData formData);
}
