package com.matevskial.dockeradmin.api;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import java.util.Set;

@Getter
@ToString
@Builder
public class UserData {
    @NonNull
    private final String id;
    private final String username;
    @ToString.Exclude
    private final String encodedPassword;
    private final Set<String> roles;
}
