package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
@Builder
public class DockerContainer {

    @NonNull String id;
    List<String> names;
    @NonNull
    String imageId;
    @NonNull
    String imageName;
    @NonNull
    String createdAt;
    @NonNull
    String status;
    @NonNull
    RunningState runningState;
    @Builder.Default
    List<DockerContainerNetwork> networks = new ArrayList<>();
    @Builder.Default
    List<DockerContainerPort> ports = new ArrayList<>();
    @Builder.Default
    List<EnvironmentVariable> environmentVariables = new ArrayList<>();
    @Builder.Default
    List<DockerContainerVolume> volumes = new ArrayList<>();

    public enum RunningState {
        RUNNING, EXITED
    }
}
