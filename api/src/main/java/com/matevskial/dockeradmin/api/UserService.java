package com.matevskial.dockeradmin.api;

import java.util.Optional;

public interface UserService {
    Optional<UserData> findByUsername(String username);
    void create(UserCreateData user);
}
