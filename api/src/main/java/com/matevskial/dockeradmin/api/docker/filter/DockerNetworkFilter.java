package com.matevskial.dockeradmin.api.docker.filter;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DockerNetworkFilter {

    String name;
    DockerNetworkDriver driver;
    boolean isEnableIpv6;
}
