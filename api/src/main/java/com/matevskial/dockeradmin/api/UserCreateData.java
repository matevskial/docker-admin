package com.matevskial.dockeradmin.api;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Set;

@Data
@Builder
@ToString
public class UserCreateData {

    private String username;
    @ToString.Exclude
    private String password;
    private Set<String> roleIds;
}
