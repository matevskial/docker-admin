package com.matevskial.dockeradmin.api.docker;

public interface DockerInfoService {

    DockerInfo getInfo();
}
