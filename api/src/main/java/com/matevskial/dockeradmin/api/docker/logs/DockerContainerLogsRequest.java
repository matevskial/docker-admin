package com.matevskial.dockeradmin.api.docker.logs;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DockerContainerLogsRequest {
    String containerId;
    int numberOfLinesToTail;
}
