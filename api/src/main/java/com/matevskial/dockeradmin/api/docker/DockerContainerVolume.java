package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class DockerContainerVolume {

    @NonNull
    String name;
    String source;
    String destination;
    String driver;
    String type;
    boolean isRw;
}
