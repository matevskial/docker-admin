package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DockerVolumeCreateResponse {

    String mountPoint;
    boolean isError;
    String errorMessage;
}
