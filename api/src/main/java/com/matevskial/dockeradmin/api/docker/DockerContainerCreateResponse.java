package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class DockerContainerCreateResponse {
    String containerId;
    boolean isError;
    String errorMessage;
}
