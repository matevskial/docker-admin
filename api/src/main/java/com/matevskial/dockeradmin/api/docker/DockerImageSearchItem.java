package com.matevskial.dockeradmin.api.docker;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class DockerImageSearchItem {
    @NonNull
    String name;
    @NonNull
    String description;
    boolean isOfficial;
    int starCount;
}
