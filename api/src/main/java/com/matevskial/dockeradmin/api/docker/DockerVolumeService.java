package com.matevskial.dockeradmin.api.docker;

import com.matevskial.dockeradmin.api.docker.filter.DockerVolumeFilter;

import java.util.List;

public interface DockerVolumeService {
    List<DockerVolume> getVolumes(DockerVolumeFilter filter);
    DockerVolumeCreateResponse createVolume(DockerVolumeCreateFormData volumeCreateFormData);
    String deleteVolume(String volumeName);
}
