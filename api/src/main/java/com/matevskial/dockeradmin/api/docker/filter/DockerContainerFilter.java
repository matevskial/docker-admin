package com.matevskial.dockeradmin.api.docker.filter;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class DockerContainerFilter {

    String containerId;
    DockerContainerRunningState runningState;
    String name;
    String imageId;
    String networkName;
}
