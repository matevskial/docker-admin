package com.matevskial.dockeradmin.security.api;

import com.matevskial.dockeradmin.api.UserData;

import java.util.Optional;

public interface SecurityService {
    void logout();
    boolean isUserLoggedIn();
    Optional<UserData> getLoggedUser();
}
