package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.model.Info;
import com.matevskial.dockeradmin.api.docker.DockerInfo;
import org.mapstruct.Mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper(componentModel = "spring")
interface DockerInfoMapper {

    default DockerInfo map(Info info) {
        return DockerInfo.builder()
                .dockerServerVersion(info.getServerVersion())
                .dockerRootDirectory(info.getDockerRootDir())
                .volumePlugins(mapPlugins("Volume", info.getPlugins()))
                .networkPlugins(mapPlugins("Network", info.getPlugins()))
                .containers(info.getContainers() == null ? 0 : info.getContainers())
                .runningContainers(info.getContainersRunning() == null ? 0 : info.getContainersRunning())
                .exitedContainers(info.getContainersStopped() == null ? 0 : info.getContainersStopped())
                .images(info.getImages() == null ? 0 : info.getImages())
                .operatingSystem(info.getOperatingSystem())
                .osType(info.getOsType())
                .kernelVersion(info.getKernelVersion())
                .hostname(info.getName())
                .architecture(info.getArchitecture())
                .numberOfCpus(info.getNCPU() == null ? 0 : info.getNCPU())
                .memory(info.getMemTotal() == null ? 0 : info.getMemTotal())
                .build();
    }

    default List<String> mapPlugins(String pluginKey, Map<String, List<String>> plugins) {
        if(plugins == null) {
            return new ArrayList<>();
        }
        return plugins.getOrDefault(pluginKey, new ArrayList<>());
    }
}
