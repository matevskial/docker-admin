package com.matevskial.dockeradmin.domain.dockercommandbuilder;

import com.matevskial.dockeradmin.api.docker.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Slf4j
public class DockerContainerRunCommandBuilder {

    private static final String DEFAULT_ENVIRONMENT_VARIABLE = "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin";
    private static final String DEFAULT_DOCKER_NETWORK_NAME = "bridge";

    public String build(DockerContainer container, DockerImage image) {
        List<String> options = List.of(getNameOption(container), getNetworkOptions(container), getMountOptions(container), getEnvironmentVariableOptions(container, image), getPortMappingOptions(container), getImage(container));
        StringBuilder sb = new StringBuilder("docker run");
        for(String option : options) {
            if(sb.length() > 0 && !option.isEmpty()) {
                sb.append(" ");
            }
            sb.append(option);
        }
        return sb.toString();
    }

    private String getNameOption(DockerContainer container) {
        if(container.getNames().isEmpty()) {
            return "";
        }
        return String.format("--name %s", container.getNames().get(0).substring(1));
    }

    private String getNetworkOptions(DockerContainer container) {
        StringBuilder sb = new StringBuilder();
        for(DockerContainerNetwork network : container.getNetworks()) {
            if(sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(getNetworkOption(network));
        }
        return sb.toString();
    }

    private String getNetworkOption(DockerContainerNetwork network) {
        if(DEFAULT_DOCKER_NETWORK_NAME.equals(network.getName())) {
            return "";
        }
        return String.format("--net %s", network.getName());
    }

    private String getMountOptions(DockerContainer container) {
        StringBuilder sb = new StringBuilder();
        sb.append(volumeDriverOptions(container));
        for(DockerContainerVolume volume : container.getVolumes()) {
            if(sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(getMountOption(volume));
        }
        return sb.toString();
    }

    private String volumeDriverOptions(DockerContainer container) {
        StringBuilder sb = new StringBuilder();
        Set<String> seenVolumeDrivers = new HashSet<>(List.of("local"));
        for(DockerContainerVolume volume : container.getVolumes()) {
            if(sb.length() > 0) {
                sb.append(" ");
            }
            if(!seenVolumeDrivers.contains(volume.getDriver())) {
                sb.append(String.format("--volume-driver %s", volume.getDriver()));
            }
            seenVolumeDrivers.add(volume.getDriver());
        }
        return sb.toString();
    }

    private String getMountOption(DockerContainerVolume volume) {
        StringBuilder sb = new StringBuilder();
        sb.append("--mount '");
        sb.append(String.format("src=%s", volume.getName()));
        sb.append(String.format(",target=%s", volume.getDestination()));
        if(!volume.isRw()){
            sb.append(",readonly");
        }
        sb.append("'");
        return sb.toString();
    }

    private String getEnvironmentVariableOptions(DockerContainer container, DockerImage image) {
        StringBuilder sb = new StringBuilder();
        Set<String> defaultEnvironmentVariables = new HashSet<>();
        for(EnvironmentVariable environmentVariable : image.getDefaultEnvironmentVariables()) {
            defaultEnvironmentVariables.add(environmentVariable.toString());
        }
        for(EnvironmentVariable environmentVariable : container.getEnvironmentVariables()) {
            if(!defaultEnvironmentVariables.contains(environmentVariable.toString())) {
                if(sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(String.format("-e %s", environmentVariable));
            }
        }
        return sb.toString();
    }

    private String getPortMappingOptions(DockerContainer container) {
        StringBuilder sb = new StringBuilder();
        Set<String> seenPortOptions = new HashSet<>();
        for(DockerContainerPort port : container.getPorts()) {
            String portOption = getPortMappingOption(port);
            if(!seenPortOptions.contains(portOption)) {
                if(sb.length() > 0) {
                    sb.append(" ");
                }
                sb.append(portOption);
            }
            seenPortOptions.add(portOption);
        }
        return sb.toString();
    }

    private String getPortMappingOption(DockerContainerPort port) {
        return String.format("-p %s:%s", port.getHostPort(), port.getContainerPort());
    }

    private String getImage(DockerContainer container) {
        return container.getImageName();
    }
}
