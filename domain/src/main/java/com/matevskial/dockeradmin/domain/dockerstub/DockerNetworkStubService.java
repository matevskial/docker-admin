package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerNetwork;
import com.matevskial.dockeradmin.api.docker.DockerNetworkCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerNetworkCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerNetworkService;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("stub")
@Slf4j
public class DockerNetworkStubService implements DockerNetworkService {

    @Override
    public List<DockerNetwork> getNetworks(DockerNetworkFilter filter) {
        log.info("Getting networks");
        return List.of(
                DockerNetwork.builder().id("36013bfc5bff6e4020da63aa874b98859f606e4395941162cf65777a057a903c").name("kanboard_default").scope("local").driver("bridge").isEnableIpv6(true).subnet("172.18.0.0/16").gateway("172.18.0.1").build()
        );
    }

    @Override
    public DockerNetworkCreateResponse createNetwork(DockerNetworkCreateFormData dockerNetworkCreateFormData) {
        return null;
    }

    @Override
    public String deleteNetwork(String networkId) {
        return null;
    }
}
