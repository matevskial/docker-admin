package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerCmd;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.model.*;
import com.matevskial.dockeradmin.api.docker.DockerContainerCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerContainerCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerContainerCreateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Profile("!stub")
@RequiredArgsConstructor
@Slf4j
class DockerContainerCreateServiceImpl implements DockerContainerCreateService {

    private final DockerClient dockerClient;

    @Override
    public DockerContainerCreateResponse createContainer(DockerContainerCreateFormData formData) {
        log.info("Creating container with configuration {}", formData);
        CreateContainerCmd cmd = buildCreateContainerCmd(formData);
        try {
            CreateContainerResponse r = cmd.exec();
            return DockerContainerCreateResponse.builder()
                    .containerId(r.getId())
                    .build();
        } catch (DockerException e) {
            return DockerContainerCreateResponse.builder()
                    .isError(true)
                    .errorMessage(e.getMessage())
                    .build();
        } catch (Exception e) {
            return DockerContainerCreateResponse.builder()
                    .isError(true)
                    .errorMessage("Unable to create container. Please contact administrators.")
                    .build();
        }
    }

    @Override
    public DockerContainerCreateResponse replaceContainer(DockerContainerCreateFormData formData) {
        log.info("Replacing container with configuration {}", formData);
        CreateContainerCmd cmd = buildCreateContainerCmd(formData);
        try {
            if(formData.getContainerId() == null) {
                throw new DockerException("Container id not specified", 500);
            }
            dockerClient.removeContainerCmd(formData.getContainerId()).exec();
            CreateContainerResponse r = cmd.exec();
            return DockerContainerCreateResponse.builder()
                    .containerId(r.getId())
                    .build();
        } catch (DockerException e) {
            return DockerContainerCreateResponse.builder()
                    .isError(true)
                    .errorMessage(e.getMessage())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
            return DockerContainerCreateResponse.builder()
                    .isError(true)
                    .errorMessage("Unable to replace container. Please contact administrators.")
                    .build();
        }
    }

    private CreateContainerCmd buildCreateContainerCmd(DockerContainerCreateFormData formData) {
        CreateContainerCmd cmd = dockerClient.createContainerCmd(formData.getImage()).withName(formData.getName());
        includePortMappings(cmd, formData);
        includeNetwork(cmd, formData);
        includeVolumes(cmd, formData);
        includeEnvironmentVariables(cmd, formData);
        return cmd;
    }

    private void includeEnvironmentVariables(CreateContainerCmd cmd, DockerContainerCreateFormData formData) {
        if(formData.getEnvironmentVariables() == null) {
            return;
        }
        List<String> environmentVariables = formData.getEnvironmentVariables().entrySet().stream()
                .map(envEntry -> String.format("%s=%s", envEntry.getKey(), envEntry.getValue()))
                .collect(Collectors.toList());
        cmd.withEnv(environmentVariables);
    }

    private void includeVolumes(CreateContainerCmd cmd, DockerContainerCreateFormData formData) {
        if(formData.getVolumes() == null) {
            return;
        }
        List<Mount> mounts = formData.getVolumes().entrySet().stream().map(volumeEntry -> new Mount()
                .withType(MountType.VOLUME)
                .withSource(volumeEntry.getKey())
                .withTarget(volumeEntry.getValue())).collect(Collectors.toList());
        HostConfig hostConfig = Objects.requireNonNullElse(cmd.getHostConfig(), HostConfig.newHostConfig());
        hostConfig.withMounts(mounts);
        cmd.withHostConfig(hostConfig);
    }

    private void includeNetwork(CreateContainerCmd cmd, DockerContainerCreateFormData formData) {
        if(!StringUtils.hasText(formData.getNetwork())) {
            return;
        }
        HostConfig hostConfig = Objects.requireNonNullElse(cmd.getHostConfig(), HostConfig.newHostConfig());
        hostConfig.withNetworkMode(formData.getNetwork());
        cmd.withHostConfig(hostConfig);
    }

    private void includePortMappings(CreateContainerCmd cmd, DockerContainerCreateFormData formData) {
        if(formData.getPortMappings() == null) {
            return;
        }

        List<PortBinding> portBindings = formData.getPortMappings().entrySet().stream()
                .map(portEntry -> new PortBinding(
                        Ports.Binding.bindPort(Integer.parseInt(portEntry.getKey())),
                        ExposedPort.tcp(Integer.parseInt(portEntry.getValue()))))
                .collect(Collectors.toList());
        HostConfig hostConfig = Objects.requireNonNullElse(cmd.getHostConfig(), HostConfig.newHostConfig());
        hostConfig.withPortBindings(portBindings);
        cmd.withHostConfig(hostConfig);
    }
}
