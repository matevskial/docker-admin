package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.matevskial.dockeradmin.api.docker.DockerException;
import com.matevskial.dockeradmin.api.docker.DockerPingService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Profile("!stub")
public class DockerPingServiceImpl implements DockerPingService {

    private final DockerClient dockerClient;

    @Override
    public void ping() throws DockerException {
        try {
            dockerClient.pingCmd().exec();
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw new DockerException("Cannot connect to the Docker daemon. Is the docker daemon running?");
        }
    }
}
