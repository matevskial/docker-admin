package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateNetworkCmd;
import com.github.dockerjava.api.command.CreateNetworkResponse;
import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.model.Network;
import com.matevskial.dockeradmin.api.docker.DockerNetwork;
import com.matevskial.dockeradmin.api.docker.DockerNetworkCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerNetworkCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerNetworkService;
import com.matevskial.dockeradmin.api.docker.filter.DockerNetworkFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Profile("!stub")
@Slf4j
@RequiredArgsConstructor
public class DockerNetworkServiceImpl implements DockerNetworkService {

    private static final String DEFAULT_NETWORK_DRIVER = "bridge";

    private final DockerClient dockerClient;
    private final DockerNetworkMapper dockerNetworkMapper;

    @Override
    public List<DockerNetwork> getNetworks(DockerNetworkFilter filter) {
        log.info("Getting networks");
        return dockerClient.listNetworksCmd().exec().stream()
                .map(dockerNetworkMapper::map)
                .filter(dockerNetwork -> doFilter(dockerNetwork, filter))
                .collect(Collectors.toList());
    }

    @Override
    public DockerNetworkCreateResponse createNetwork(DockerNetworkCreateFormData dockerNetworkCreateFormData) {
        CreateNetworkCmd cmd = dockerClient.createNetworkCmd();
        cmd.withName(dockerNetworkCreateFormData.getName());
        cmd.withDriver(Objects.requireNonNullElse(dockerNetworkCreateFormData.getDriver(), DEFAULT_NETWORK_DRIVER));

        if(dockerNetworkCreateFormData.getDriverOptions() != null) {
            cmd.withOptions(dockerNetworkCreateFormData.getDriverOptions());
        }

        Network.Ipam.Config config = new Network.Ipam.Config();

        if(StringUtils.hasText(dockerNetworkCreateFormData.getSubnet())) {
            config.withSubnet(dockerNetworkCreateFormData.getSubnet());
        }
        if(StringUtils.hasText(dockerNetworkCreateFormData.getGateway())) {
            config.withGateway(dockerNetworkCreateFormData.getGateway());
        }

        Network.Ipam ipam = new Network.Ipam();
        ipam.withConfig(config);
        if(StringUtils.hasText(dockerNetworkCreateFormData.getSubnet())) {
            cmd.withIpam(ipam);
        }

        try {
            CreateNetworkResponse r = cmd.exec();
            return DockerNetworkCreateResponse.builder()
                    .networkId(r.getId())
                    .build();
        } catch (DockerException e) {
            return DockerNetworkCreateResponse.builder()
                    .isError(true)
                    .errorMessage(e.getMessage())
                    .build();
        }
        catch (Exception e) {
            return DockerNetworkCreateResponse.builder()
                    .isError(true)
                    .errorMessage("Unable to create volume. Please contact administrators.")
                    .build();
        }
    }

    @Override
    public String deleteNetwork(String networkId) {
        try {
            dockerClient.removeNetworkCmd(networkId).exec();
        } catch (DockerException e) {
            return e.getMessage();
        } catch (Exception e) {
            return String.format("Unable to remove network with id %s. Please contact administrators.", networkId);
        }
        return null;
    }

    private boolean doFilter(DockerNetwork dockerNetwork, DockerNetworkFilter filter) {
        boolean hasName = !StringUtils.hasText(filter.getName()) || dockerNetwork.getName().contains(filter.getName());
        boolean hasDriver = filter.getDriver() == null || dockerNetwork.getDriver().equalsIgnoreCase(filter.getDriver().name());
        boolean hasEnableIpv6 = filter.isEnableIpv6() == dockerNetwork.isEnableIpv6();
        return hasName && hasDriver && hasEnableIpv6;
    }
}
