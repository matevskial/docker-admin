package com.matevskial.dockeradmin.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")
@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
class UserEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    @Setter
    private String id;

    @Setter
    private String username;

    @Setter
    private String encodedPassword;

    @Setter
    @OneToMany
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<RoleEntity> roles;

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof UserEntity && id != null && id.equals(((UserEntity) o).getId()));
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
