package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerVolume;
import com.matevskial.dockeradmin.api.docker.DockerVolumeCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerVolumeCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerVolumeService;
import com.matevskial.dockeradmin.api.docker.filter.DockerVolumeFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("stub")
@Slf4j
public class DockerVolumeStubService implements DockerVolumeService {

    @Override
    public List<DockerVolume> getVolumes(DockerVolumeFilter filter) {
        log.info("Getting volumes");
        return List.of(
                DockerVolume.builder().name("kanboard_kanboard_data").driver("local").mountPoint("/var/lib/docker/volumes/kanboard_kanboard_data/_data").build()
        );
    }

    @Override
    public DockerVolumeCreateResponse createVolume(DockerVolumeCreateFormData volumeCreateFormData) {
        return null;
    }

    @Override
    public String deleteVolume(String volumeName) {
        return null;
    }
}
