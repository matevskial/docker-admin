package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.command.InspectImageResponse;
import com.github.dockerjava.api.model.ContainerConfig;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.SearchItem;
import com.matevskial.dockeradmin.api.docker.DockerImage;
import com.matevskial.dockeradmin.api.docker.DockerImageSearchItem;
import com.matevskial.dockeradmin.api.docker.EnvironmentVariable;
import org.mapstruct.Mapper;

import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
interface DockerImageMapper {

    default List<DockerImage> mapToDockerImages(List<InspectImageResponse> imageResponseList) {
        return imageResponseList.stream().map(this::map).collect(Collectors.toList());
    }

    default DockerImage map(InspectImageResponse inspectImageResponse) {
        return DockerImage.builder()
                .id(Objects.requireNonNull(inspectImageResponse.getId()))
                .createdAt(Objects.requireNonNull(inspectImageResponse.getCreated()))
                .repoTag(mapRepoTag(inspectImageResponse))
                .cmd(mapCmd(inspectImageResponse))
                .entryPoint(mapEntryPoint(inspectImageResponse))
                .exposedPorts(mapExposedPort(inspectImageResponse))
                .volumes(mapVolumes(inspectImageResponse))
                .defaultEnvironmentVariables(mapDefaultEnvironmentVariables(inspectImageResponse))
                .operatingSystem(inspectImageResponse.getOs())
                .architecture(inspectImageResponse.getArch())
                .build();
    }

    default String mapRepoTag(InspectImageResponse inspectImageResponse) {
        List<String> repoTags = inspectImageResponse.getRepoTags();
        if(repoTags == null || repoTags.isEmpty()) {
            return "";
        }
        return repoTags.get(0);
    }

    default List<String> mapCmd(InspectImageResponse inspectImageResponse) {
        ContainerConfig containerConfig = inspectImageResponse.getConfig();
        if(containerConfig == null) {
            return mapNullableArrayToList(null);
        }
        return mapNullableArrayToList(containerConfig.getCmd());
    }

    default List<String> mapEntryPoint(InspectImageResponse inspectImageResponse) {
        ContainerConfig containerConfig = inspectImageResponse.getConfig();
        if(containerConfig == null) {
            return mapNullableArrayToList(null);
        }
        return mapNullableArrayToList(containerConfig.getEntrypoint());
    }

    default List<String> mapNullableArrayToList(String[] cmd) {
        if(cmd == null) {
            return List.of();
        } else {
            return Arrays.asList(cmd);
        }
    }

    default List<String> mapExposedPort(InspectImageResponse inspectImageResponse) {
        ExposedPort[] exposedPorts = null;
        ContainerConfig containerConfig = inspectImageResponse.getConfig();
        if(containerConfig != null) {
            exposedPorts = containerConfig.getExposedPorts();
        }

        if(exposedPorts == null) {
            return new ArrayList<>();
        }
        List<String> result = new ArrayList<>();
        for(ExposedPort exposedPort : exposedPorts) {
            result.add(exposedPort.getPort() + " " + exposedPort.getProtocol());
        }
        return result;
    }

    default List<String> mapVolumes(InspectImageResponse inspectImageResponse) {
        Map<String, ?> volumes = null;
        ContainerConfig containerConfig = inspectImageResponse.getConfig();
        if(containerConfig != null) {
            volumes = containerConfig.getVolumes();
        }

        if(volumes == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(volumes.keySet());
    }

    default List<EnvironmentVariable> mapDefaultEnvironmentVariables(InspectImageResponse inspectImageResponse) {
        if(inspectImageResponse.getConfig() == null) {
            return new ArrayList<>();
        }
        String[] envs = inspectImageResponse.getConfig().getEnv();
        if(envs == null) {
            return new ArrayList<>();
        }
        List<EnvironmentVariable> environmentVariables = new ArrayList<>();
        for(String env : envs) {
            environmentVariables.add(mapEnvironmentVariable(env));
        }
        return environmentVariables;
    }

    default EnvironmentVariable mapEnvironmentVariable(String env) {
        String[] parts = env.split("=");
        return EnvironmentVariable.builder()
                .name(parts[0])
                .value(parts[1])
                .build();
    }

    default List<DockerImageSearchItem> mapToDockerImageSearchItems(List<SearchItem> searchItems) {
        return searchItems.stream().map(this::map).collect(Collectors.toList());
    }

    default DockerImageSearchItem map(SearchItem searchItem) {
        return DockerImageSearchItem.builder()
                .name(searchItem.getName())
                .description(searchItem.getDescription())
                .isOfficial(searchItem.isOfficial() != null && searchItem.isOfficial())
                .starCount(searchItem.getStarCount() == null ? 0 : searchItem.getStarCount())
                .build();
    }
}
