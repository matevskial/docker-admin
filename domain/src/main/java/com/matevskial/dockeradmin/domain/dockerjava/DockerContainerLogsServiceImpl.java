package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.async.ResultCallback;
import com.github.dockerjava.api.exception.NotFoundException;
import com.github.dockerjava.api.model.Frame;
import com.matevskial.dockeradmin.api.docker.DockerContainerLogsService;
import com.matevskial.dockeradmin.api.docker.logs.DockerContainerLogsRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Profile("!stub")
@RequiredArgsConstructor
@Slf4j
public class DockerContainerLogsServiceImpl implements DockerContainerLogsService {

    private final DockerClient dockerClient;

    @Override
    public List<String> getLogs(DockerContainerLogsRequest logsRequest) {
        List<String> logLines = new ArrayList<>();
        log.info("Getting logs for container with id {}", logsRequest.getContainerId());
        try {
            System.out.println("the logs");
            ResultCallback.Adapter<Frame> r = new ResultCallback.Adapter<>() {
                @Override
                public void onNext(Frame f) {
                    logLines.add(new String(f.getPayload()));
                }
            };
            dockerClient.logContainerCmd(logsRequest.getContainerId()).withStdOut(true).withTail(logsRequest.getNumberOfLinesToTail()).withStdErr(true).exec(r).awaitCompletion();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (NotFoundException e) {
            return List.of();
        }
        return logLines;
    }
}
