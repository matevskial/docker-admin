package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.matevskial.dockeradmin.api.docker.*;
import com.matevskial.dockeradmin.api.docker.filter.DockerContainerFilter;
import com.matevskial.dockeradmin.api.docker.filter.DockerContainerRunningState;
import com.matevskial.dockeradmin.domain.dockercommandbuilder.DockerContainerRunCommandBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Profile("!stub")
@RequiredArgsConstructor
@Slf4j
class DockerContainerServiceImpl implements DockerContainerService {

    private final DockerClient dockerClient;
    private final DockerContainerMapper dockerContainerMapper;
    private final DockerImageMapper dockerImageMapper;
    private final DockerContainerRunCommandBuilder dockerContainerRunCommandBuilder;

    @Override
    public List<DockerContainer> getAllContainers() {
        return List.of();
    }

    @Override
    public List<DockerContainer> getRunningContainers() {
        return List.of();
    }

    @Override
    public List<DockerContainer> getContainers(DockerContainerFilter filter) {
        List<Container> allContainers = dockerClient.listContainersCmd().withShowAll(true).exec();
        return allContainers.stream()
                .map(container -> DockerContainerPair.builder()
                        .container(container)
                        .inspectContainerResponse(dockerClient.inspectContainerCmd(container.getId()).exec())
                        .build())
                .map(dockerContainerMapper::map)
                .filter(container -> doFilter(container, filter))
                .collect(Collectors.toList());
    }

    @Override
    public String startContainer(String containerId) {
        try {
            dockerClient.startContainerCmd(containerId).exec();
        } catch (DockerException e) {
            return e.getMessage();
        } catch (Exception e) {
            return String.format("Unable to start container with id %s. Please contact administrators", containerId);
        }
        return null;
    }

    @Override
    public String stopContainer(String containerId) {
        try {
            dockerClient.stopContainerCmd(containerId).exec();
        } catch (DockerException e) {
            return e.getMessage();
        } catch (Exception e) {
            return String.format("Unable to stop container with id %s. Please contact administrators", containerId);
        }
        return null;
    }

    @Override
    public String deleteContainer(String containerId, boolean deleteVolumes) {
        try {
            dockerClient.removeContainerCmd(containerId).withRemoveVolumes(deleteVolumes).exec();
        } catch (DockerException e) {
            return e.getMessage();
        } catch (Exception e) {
            return String.format("Unable to remove container with id %s. Please contact administrators", containerId);
        }
        return null;
    }

    @Override
    public String buildDockerRunCommand(String containerId) {
        log.info("Building docker run command for container with id {}", containerId);
        DockerContainer container = getContainer(containerId);
        if(container == null) {
            return "";
        }
        DockerImage image = getImage(container.getImageName());
        if(image == null) {
            return "";
        }
        return dockerContainerRunCommandBuilder.build(container, image);
    }

    private DockerImage getImage(String imageName) {
        List<Image> images = dockerClient.listImagesCmd().withImageNameFilter(imageName).exec();
        if(images == null || images.isEmpty()) {
            return null;
        }
        return dockerImageMapper.map(dockerClient.inspectImageCmd(images.get(0).getId()).exec());
    }

    private DockerContainer getContainer(String containerId) {
        List<Container> containers = dockerClient.listContainersCmd().withShowAll(true).withIdFilter(List.of(containerId)).exec();
        if(containers == null || containers.isEmpty()) {
            return null;
        }

        InspectContainerResponse inspectContainerResponse = dockerClient.inspectContainerCmd(containerId).exec();
        return dockerContainerMapper.map(DockerContainerPair.builder().container(containers.get(0)).inspectContainerResponse(inspectContainerResponse).build());
    }

    private boolean doFilter(DockerContainer container, DockerContainerFilter filter) {
        boolean hasId = filter.getContainerId() == null || container.getId().equals(filter.getContainerId());
        boolean isAll = filter.getRunningState() == null || filter.getRunningState().equals(DockerContainerRunningState.ALL);
        boolean hasStatus = filter.getRunningState() == null || filter.getRunningState().name().equalsIgnoreCase(container.getRunningState().name());
        boolean containsTextInName = !StringUtils.hasText(filter.getName()) || container.getNames().get(0).contains(filter.getName().trim());
        boolean hasImage = !StringUtils.hasText(filter.getImageId()) || container.getImageId().equals(filter.getImageId());
        boolean hasNetwork = hasNetwork(container, filter);
        return hasId && (isAll || hasStatus) && containsTextInName && hasImage && hasNetwork;
    }

    private boolean hasNetwork(DockerContainer container, DockerContainerFilter filter) {
        if(!StringUtils.hasText(filter.getNetworkName())) {
            return true;
        }

        return container.getNetworks().stream().anyMatch(
                dockerContainerNetwork -> dockerContainerNetwork.getName().contains(filter.getNetworkName()));
    }

    private DockerContainerVolume createVolume(String volumeName) {
        return DockerContainerVolume.builder().name(volumeName)
                .source("/var/lib/docker/volumes/kanboard_kanboard_ssl/_data")
                .destination("/etc/nginx/ssl")
                .build();
    }

    private List<EnvironmentVariable> createEnvironmentVariables() {
        return List.of(
                EnvironmentVariable.builder().name("PASSWORD").value("123").build(),
                EnvironmentVariable.builder().name("USER").value("user").build()
        );
    }

    private DockerContainerPort createPort(String containerPort) {
        return DockerContainerPort.builder().containerPort(containerPort).hostPort("8080").hostIp("0.0.0.0").protocol("tcp").build();
    }

    private DockerContainerNetwork createNetwork(String networkName) {
        return DockerContainerNetwork.builder().id("123").name(networkName).gateway("192.168.0.1").containerIpAddress("192.168.0.19").driver("bridge").build();
    }

    private Instant getInstant(String instant) {
        return Instant.parse(instant);
    }
}
