package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.model.Network;
import com.matevskial.dockeradmin.api.docker.DockerNetwork;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
interface DockerNetworkMapper {

    default List<DockerNetwork> map(List<Network> networks) {
        return networks.stream().map(this::map).collect(Collectors.toList());
    }

    default DockerNetwork map(Network network) {
        return DockerNetwork.builder()
                .id(network.getId())
                .name(network.getName())
                .scope(network.getScope())
                .subnet(mapSubnet(network.getIpam().getConfig()))
                .gateway(mapGateway(network.getIpam().getConfig()))
                .driver(network.getDriver())
                .isEnableIpv6(network.getEnableIPv6())
                .driverOptions(network.getOptions())
                .build();
    }

    default String mapSubnet(List<Network.Ipam.Config> config) {
        if(config.isEmpty()) {
            return "";
        } else {
            return config.get(0).getSubnet();
        }
    }

    default String mapGateway(List<Network.Ipam.Config> config) {
        if(config.isEmpty()) {
            return "";
        } else {
            return config.get(0).getGateway();
        }
    }
}
