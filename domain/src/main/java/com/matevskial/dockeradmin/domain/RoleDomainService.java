package com.matevskial.dockeradmin.domain;

import com.matevskial.dockeradmin.api.RoleCreateData;
import com.matevskial.dockeradmin.api.RoleData;
import com.matevskial.dockeradmin.api.RoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
class RoleDomainService implements RoleService {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    @Override
    @Transactional(readOnly = true)
    public Optional<RoleData> findByName(String roleName) {
        log.info("Finding role by name: {}", roleName);
        return roleRepository.findByName(roleName).map(roleMapper::toData);
    }

    @Override
    @Transactional
    public RoleData create(RoleCreateData role) {
        log.info("Creating role: {}", role);
        RoleEntity roleToCreate = roleMapper.toEntity(role);
        return roleMapper.toData(roleRepository.save(roleToCreate));
    }
}
