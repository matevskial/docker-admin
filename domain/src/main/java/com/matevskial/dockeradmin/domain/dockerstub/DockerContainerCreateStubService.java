package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerContainerCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerContainerCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerContainerCreateService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("stub")
class DockerContainerCreateStubService implements DockerContainerCreateService {

    @Override
    public DockerContainerCreateResponse createContainer(DockerContainerCreateFormData formData) {
        return null;
    }

    @Override
    public DockerContainerCreateResponse replaceContainer(DockerContainerCreateFormData formData) {
        return null;
    }
}
