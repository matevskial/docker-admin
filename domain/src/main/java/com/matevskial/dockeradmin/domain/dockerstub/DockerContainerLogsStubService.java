package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerContainerLogsService;
import com.matevskial.dockeradmin.api.docker.logs.DockerContainerLogsRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("stub")
public class DockerContainerLogsStubService implements DockerContainerLogsService {

    @Override
    public List<String> getLogs(DockerContainerLogsRequest request) {
        return List.of(
                "Hello from Docker!",
                "This message shows that your installation appears to be working correctly.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                "To generate this message, Docker took the following steps:",
                "abc",
                "efg",
                "123",
                "xyz",
                "fdgfg",
                "bfg",
                "abc",
                "efg",
                "123",
                "xyz",
                "fdgfg",
                "bfg"
        );
    }
}
