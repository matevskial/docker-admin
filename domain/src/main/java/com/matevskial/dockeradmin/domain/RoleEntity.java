package com.matevskial.dockeradmin.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "role")
@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
class RoleEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(length = 36)
    @Setter
    private String id;

    @Setter
    private String name;

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof RoleEntity && id != null && id.equals(((RoleEntity) o).getId()));
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
