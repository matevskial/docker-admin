package com.matevskial.dockeradmin.domain;

import com.matevskial.dockeradmin.api.UserCreateData;
import com.matevskial.dockeradmin.api.UserData;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = PasswordEncoderMapper.class)
interface UserMapper {

    @Mapping(source = "roles", target = "roles", qualifiedByName = "mapRoles")
    UserData toData(UserEntity user);

    @Named("mapRoles")
    default Set<String> mapRoles(Collection<RoleEntity> roles) {
        return roles.stream().map(RoleEntity::getName).collect(Collectors.toSet());
    }

    @Named("mapRoleIds")
    default Set<RoleEntity> mapRoleIds(Collection<String> roleIds) {
        return roleIds.stream().map(roleId -> RoleEntity.builder().id(roleId).build()).collect(Collectors.toSet());
    }

    @Mapping(source = "password", target = "encodedPassword", qualifiedBy = EncodedMapping.class)
    @Mapping(source = "roleIds", target = "roles", qualifiedByName = "mapRoleIds")
    @Mapping(target = "id", ignore = true)
    UserEntity toEntity(UserCreateData user);
}
