package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerImage;
import com.matevskial.dockeradmin.api.docker.DockerImageSearchItem;
import com.matevskial.dockeradmin.api.docker.DockerImageService;
import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@Profile("stub")
public class DockerImageStubService implements DockerImageService {

    @Override
    public List<DockerImage> getImages(DockerImageFilter filter) {
        log.info("Getting images");
        return List.of(
                DockerImage.builder().id("sha256:e66264b98777e12192600bf9b4d663655c98a090072e1bab49e233d7531d1294").createdAt("2021-09-23T23:47:57.442225064Z").volumes(List.of("/etc/data", "/var/data")).entryPoint(List.of("/usr/bin/java")).repoTag("alpine:3.16.0").operatingSystem("linux").architecture("amd64").build(),
                DockerImage.builder().id("sha256:f7f5faa373dfbcc9f9405ce11f1964177c6a72a5c7a797852de15d0b36f655fb").createdAt("2021-09-23T23:47:57.442225064Z").repoTag("kanboard/kanboard:v1.2.22").operatingSystem("linux").architecture("amd64").build()
        );
    }

    @Override
    public List<DockerImageSearchItem> searchImages(String searchTerm) {
        return List.of(
                DockerImageSearchItem.builder().name("mongo").description("MongoDB document databases provide high availability and easy scalability.").starCount(8864).isOfficial(true).build(),
                DockerImageSearchItem.builder().name("dpage/pgadmin4").description("pgAdmin 4 is a web based administration tool for the PostgreSQL database.").starCount(940).isOfficial(false).build()
        );
    }
}
