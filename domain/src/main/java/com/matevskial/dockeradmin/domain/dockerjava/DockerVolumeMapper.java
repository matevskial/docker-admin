package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.command.InspectVolumeResponse;
import com.matevskial.dockeradmin.api.docker.DockerVolume;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
interface DockerVolumeMapper {

    default List<DockerVolume> map(List<InspectVolumeResponse> inspectVolumeResponses) {
        return inspectVolumeResponses.stream().map(this::map).collect(Collectors.toList());
    }

    default DockerVolume map(InspectVolumeResponse inspectVolumeResponse) {
        return DockerVolume.builder()
                .name(inspectVolumeResponse.getName())
                .driver(inspectVolumeResponse.getDriver())
                .mountPoint(inspectVolumeResponse.getMountpoint())
                .driverOptions(inspectVolumeResponse.getOptions())
                .build();
    }
}
