package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateVolumeCmd;
import com.github.dockerjava.api.command.CreateVolumeResponse;
import com.github.dockerjava.api.exception.DockerException;
import com.matevskial.dockeradmin.api.docker.DockerVolume;
import com.matevskial.dockeradmin.api.docker.DockerVolumeCreateFormData;
import com.matevskial.dockeradmin.api.docker.DockerVolumeCreateResponse;
import com.matevskial.dockeradmin.api.docker.DockerVolumeService;
import com.matevskial.dockeradmin.api.docker.filter.DockerVolumeFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Profile("!stub")
@Slf4j
@RequiredArgsConstructor
public class DockerVolumeServiceImpl implements DockerVolumeService {

    private final DockerClient dockerClient;
    private final DockerVolumeMapper dockerVolumeMapper;

    @Override
    public List<DockerVolume> getVolumes(DockerVolumeFilter filter) {
        log.info("Getting volumes");
        return dockerClient.listVolumesCmd().exec().getVolumes().stream()
                .map(dockerVolumeMapper::map)
                .filter(dockerVolume -> doFilter(dockerVolume, filter))
                .collect(Collectors.toList());
    }

    @Override
    public DockerVolumeCreateResponse createVolume(DockerVolumeCreateFormData volumeCreateFormData) {
        log.info("Creating {}", volumeCreateFormData);
        CreateVolumeCmd cmd = dockerClient.createVolumeCmd();
        cmd.withName(volumeCreateFormData.getName());
        cmd.withDriver(volumeCreateFormData.getDriver());
        if(volumeCreateFormData.getDriverOptions() != null) {
            cmd.withDriverOpts(volumeCreateFormData.getDriverOptions());
        }

        try {
            CreateVolumeResponse r = cmd.exec();
            return DockerVolumeCreateResponse.builder()
                    .mountPoint(r.getMountpoint())
                    .build();
        } catch (DockerException e) {
            return DockerVolumeCreateResponse.builder()
                    .isError(true)
                    .errorMessage(e.getMessage())
                    .build();
        } catch (Exception e) {
            return DockerVolumeCreateResponse.builder()
                    .isError(true)
                    .errorMessage("Unable to create volume. Please contact administrators.")
                    .build();
        }
    }

    @Override
    public String deleteVolume(String volumeName) {
        try {
            dockerClient.removeVolumeCmd(volumeName).exec();
        } catch (DockerException e) {
            return e.getMessage();
        } catch (Exception e) {
            return String.format("Unable to remove volume with name %s. Please contact administrators", volumeName);
        }
        return null;
    }

    private boolean doFilter(DockerVolume dockerVolume, DockerVolumeFilter filter) {
        return !StringUtils.hasText(filter.getName()) || dockerVolume.getName().contains(filter.getName());
    }
}
