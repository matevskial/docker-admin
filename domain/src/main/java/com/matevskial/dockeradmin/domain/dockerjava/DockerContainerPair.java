package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.command.InspectContainerResponse;
import com.github.dockerjava.api.model.Container;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
class DockerContainerPair {
    Container container;
    InspectContainerResponse inspectContainerResponse;
}
