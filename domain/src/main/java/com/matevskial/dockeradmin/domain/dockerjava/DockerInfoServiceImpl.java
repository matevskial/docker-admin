package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.matevskial.dockeradmin.api.docker.DockerInfo;
import com.matevskial.dockeradmin.api.docker.DockerInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("!stub")
@Slf4j
@RequiredArgsConstructor
class DockerInfoServiceImpl implements DockerInfoService {

    private final DockerClient dockerClient;
    private final DockerInfoMapper dockerInfoMapper;

    @Override
    public DockerInfo getInfo() {
        log.info("Getting docker system info");
        return dockerInfoMapper.map(dockerClient.infoCmd().exec());
    }
}
