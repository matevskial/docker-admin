package com.matevskial.dockeradmin.domain;

import com.matevskial.dockeradmin.api.UserCreateData;
import com.matevskial.dockeradmin.api.UserData;
import com.matevskial.dockeradmin.api.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
class UserDomainService implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder encoder;

    @Override
    @Transactional(readOnly = true)
    public Optional<UserData> findByUsername(String username) {
        log.info("Finding user by username: {}", username);
        return userRepository.findByUsername(username).map(userMapper::toData);
    }

    @Override
    @Transactional
    public void create(UserCreateData user) {
        log.info("Creating user: {}", user);
        userRepository.save(userMapper.toEntity(user));
    }
}
