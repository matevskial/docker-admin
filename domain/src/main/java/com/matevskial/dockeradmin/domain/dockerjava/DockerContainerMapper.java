package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.model.*;
import com.matevskial.dockeradmin.api.docker.*;
import org.mapstruct.Mapper;

import java.util.*;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
interface DockerContainerMapper {

    default List<DockerContainer> map(List<DockerContainerPair> containerPair) {
        return containerPair.stream().map(this::map).collect(Collectors.toList());
    }

    default DockerContainer map(DockerContainerPair containerPair) {
        return DockerContainer.builder()
                .id(containerPair.getContainer().getId())
                .names(Arrays.asList(containerPair.getContainer().getNames()))
                .imageId(containerPair.getInspectContainerResponse().getImageId())
                .imageName(containerPair.getContainer().getImage())
                .createdAt(containerPair.getInspectContainerResponse().getCreated())
                .status(containerPair.getContainer().getStatus())
                .runningState(mapRunningState(containerPair.getContainer().getState()))
                .networks(mapNetworks(containerPair.getContainer().getNetworkSettings()))
                .ports(mapPorts(containerPair))
                .environmentVariables(mapEnvironmentVariables(containerPair.getInspectContainerResponse().getConfig().getEnv()))
                .volumes(mapVolumes(containerPair.getContainer().getMounts()))
                .build();
    }

    default DockerContainer.RunningState mapRunningState(String state) {
        if("running".equals(state)) {
            return DockerContainer.RunningState.RUNNING;
        } else {
            return DockerContainer.RunningState.EXITED;
        }
    }

    default List<DockerContainerNetwork> mapNetworks(ContainerNetworkSettings containerNetworkSettings) {
        if(containerNetworkSettings == null) {
            return new ArrayList<>();
        }
        List<DockerContainerNetwork> containerNetworks = new ArrayList<>();
        for(Map.Entry<String, ContainerNetwork> entry : containerNetworkSettings.getNetworks().entrySet()) {
            containerNetworks.add(mapNetwork(entry.getKey(), entry.getValue()));
        }
        return containerNetworks;
    }

    default DockerContainerNetwork mapNetwork(String networkName, ContainerNetwork containerNetwork) {
        return DockerContainerNetwork.builder()
                .id(Objects.requireNonNull(containerNetwork.getNetworkID()))
                .name(networkName)
                .gateway(containerNetwork.getGateway())
                .containerIpAddress(containerNetwork.getIpAddress())
                .driver("bridge")
                .build();
    }

    default List<DockerContainerPort> mapPorts(DockerContainerPair containerPair) {
        if("running".equals(containerPair.getContainer().getState())) {
            return mapPortsForRunningContainer(containerPair.getContainer().getPorts());
        }
        return mapPortsForExitedContainer(containerPair.getInspectContainerResponse().getHostConfig().getPortBindings());
    }

    default List<DockerContainerPort> mapPortsForRunningContainer(ContainerPort[] ports) {
        List<DockerContainerPort> dockerContainerPorts = new ArrayList<>();
        for(ContainerPort port : ports) {
            if(port.getPublicPort() != null) {
                dockerContainerPorts.add(mapPort(port));
            }
        }
        return dockerContainerPorts;
    }

    default List<DockerContainerPort> mapPortsForExitedContainer(Ports ports) {
        List<DockerContainerPort> dockerContainerPorts = new ArrayList<>();
        if(ports == null) {
            return dockerContainerPorts;
        }
        for(Map.Entry<ExposedPort, Ports.Binding[]> entry : ports.getBindings().entrySet()) {
            for(Ports.Binding portBinding : entry.getValue()) {
                dockerContainerPorts.add(mapPort(entry.getKey(), portBinding));
            }
        }
        return dockerContainerPorts;
    }

    default DockerContainerPort mapPort(ExposedPort exposedPort, Ports.Binding portBinding) {
        return DockerContainerPort.builder()
                .containerPort(String.valueOf(exposedPort.getPort()))
                .hostPort(portBinding.getHostPortSpec())
                .protocol(exposedPort.getProtocol().name().toLowerCase())
                .build();
    }

    default DockerContainerPort mapPort(ContainerPort port) {
        return DockerContainerPort.builder()
                .containerPort(String.valueOf(port.getPrivatePort()))
                .hostPort(String.valueOf(port.getPublicPort()))
                .hostIp(port.getIp())
                .protocol(Objects.requireNonNullElse(port.getType(), ""))
                .build();
    }

    default List<EnvironmentVariable> mapEnvironmentVariables(String[] envs) {
        if(envs == null) {
            return new ArrayList<>();
        }
        List<EnvironmentVariable> environmentVariables = new ArrayList<>();
        for(String env : envs) {
            environmentVariables.add(mapEnvironmentVariable(env));
        }
        return environmentVariables;
    }

    default EnvironmentVariable mapEnvironmentVariable(String env) {
        String[] parts = env.split("=");
        return EnvironmentVariable.builder()
                .name(parts[0])
                .value(parts[1])
                .build();
    }

    default List<DockerContainerVolume> mapVolumes(List<ContainerMount> mounts) {
        List<DockerContainerVolume> volumes = new ArrayList<>();
        for(ContainerMount mount : mounts) {
            volumes.add(mapVolume(mount));
        }
        return volumes;
    }

    default DockerContainerVolume mapVolume(ContainerMount mount) {
        String name = mount.getName() == null ? "(bind-mount)" : mount.getName();
        return DockerContainerVolume.builder()
                .name(name)
                .source(mount.getSource())
                .destination(mount.getDestination())
                .driver(mount.getDriver())
                /*
                docker-java does not give the type data for a mount
                but docker-java will set name of the mount to null
                if the type of the mount is not 'volume'
                 */
                .type(mount.getName() == null ? "bind" : "volume")
                .isRw(Objects.requireNonNullElse(mount.getRw(), false))
                .build();
    }
}
