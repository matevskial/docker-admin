package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerException;
import com.matevskial.dockeradmin.api.docker.DockerPingService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("stub")
class DockerPingStubService implements DockerPingService {

    @Override
    public void ping() throws DockerException {
        // stub
    }
}
