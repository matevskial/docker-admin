package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.DockerInfo;
import com.matevskial.dockeradmin.api.docker.DockerInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("stub")
@Slf4j
class DockerInfoStubService implements DockerInfoService {

    @Override
    public DockerInfo getInfo() {
        return DockerInfo.builder()
                .dockerServerVersion("20.10.17")
                .dockerRootDirectory("/var/lib/docker")
                .volumePlugins(List.of("local"))
                .networkPlugins(List.of("bridge", "host", "ipvlan", "macvlan"))
                .containers(8)
                .runningContainers(1)
                .exitedContainers(7)
                .images(4)
                .operatingSystem("KDE neon User - 5.24")
                .osType("linux")
                .kernelVersion("5.13.0-48-generic")
                .hostname("kvm5")
                .architecture("X86_64")
                .numberOfCpus(12)
                .memory(16699928576L)
                .build();
    }
}
