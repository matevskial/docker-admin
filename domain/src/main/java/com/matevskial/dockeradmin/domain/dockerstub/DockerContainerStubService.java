package com.matevskial.dockeradmin.domain.dockerstub;

import com.matevskial.dockeradmin.api.docker.*;
import com.matevskial.dockeradmin.api.docker.filter.DockerContainerFilter;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Profile("stub")
class DockerContainerStubService implements DockerContainerService {

    @Override
    public List<DockerContainer> getAllContainers() {
        return List.of(
                DockerContainer.builder()
                        .id("b8ce8762fdaf523b7f4c1a3ba25a5ed6f5c9c3705e6990f1baf30c379275a834")
                        .names(List.of("hello-world-container"))
                        .imageName("hello-world")
                        .status("Exited (0) 7 weeks ago")
                        .createdAt("2022-04-02T12:09:44.530247189Z")
                        .build()

        );
    }

    @Override
    public List<DockerContainer> getRunningContainers() {
        return List.of(
                DockerContainer.builder()
                        .id("b8ce8762fdaf523b7f4c1a3ba25a5ed6f5c9c3705e6990f1baf30c379275a834")
                        .names(List.of("hello-world-container"))
                        .imageName("hello-world")
                        .status("Up About an hour")
                        .runningState(DockerContainer.RunningState.RUNNING)
                        .createdAt("2022-04-02T12:09:44.530247189Z")
                        .networks(List.of(createNetwork("network1"), createNetwork("network2")))
                        .ports(List.of(createPort("80"), createPort("443")))
                        .environmentVariables(createEnvironmentVariables())
                        .volumes(List.of(createVolume("app_ssl"), createVolume("app_data")))
                        .build(),
                DockerContainer.builder()
                        .id("a12435tghfghfhc1a3ba25a5ed6f5c9c3705e6990f1baf30c379275a834")
                        .names(List.of("hello-alpine-container"))
                        .imageName("hello-alpine-image")
                        .status("Up 2 hours")
                        .runningState(DockerContainer.RunningState.EXITED)
                        .createdAt("2022-04-02T12:09:44.530247189Z")
                        .build()
        );
    }

    @Override
    public List<DockerContainer> getContainers(DockerContainerFilter filter) {
        return List.of(
                DockerContainer.builder()
                        .id("b8ce8762fdaf523b7f4c1a3ba25a5ed6f5c9c3705e6990f1baf30c379275a834")
                        .names(List.of("hello-world-container"))
                        .imageName("hello-world")
                        .status("Up About an hour")
                        .runningState(DockerContainer.RunningState.RUNNING)
                        .createdAt("2022-04-02T12:09:44.530247189Z")
                        .networks(List.of(createNetwork("network1"), createNetwork("network2")))
                        .ports(List.of(createPort("80"), createPort("443")))
                        .environmentVariables(createEnvironmentVariables())
                        .volumes(List.of(createVolume("app_ssl"), createVolume("app_data")))
                        .build(),
                DockerContainer.builder()
                        .id("a12435tghfghfhc1a3ba25a5ed6f5c9c3705e6990f1baf30c379275a834")
                        .names(List.of("hello-alpine-container"))
                        .imageName("hello-alpine-image")
                        .status("Up 2 hours")
                        .runningState(DockerContainer.RunningState.EXITED)
                        .createdAt("2022-04-02T12:09:44.530247189Z")
                        .build()
        );
    }

    @Override
    public String startContainer(String containerId) {
        return null;
    }

    @Override
    public String stopContainer(String containerId) {
        return null;
    }

    @Override
    public String deleteContainer(String containerId, boolean deleteVolumes) {
        return null;
    }

    @Override
    public String buildDockerRunCommand(String containerId) {
        return "docker run";
    }

    private DockerContainerVolume createVolume(String volumeName) {
        return DockerContainerVolume.builder().name(volumeName)
                .source("/var/lib/docker/volumes/kanboard_kanboard_ssl/_data")
                .destination("/etc/nginx/ssl")
                .build();
    }

    private List<EnvironmentVariable> createEnvironmentVariables() {
        return List.of(
                EnvironmentVariable.builder().name("PASSWORD").value("123").build(),
                EnvironmentVariable.builder().name("USER").value("user").build()
        );
    }

    private DockerContainerPort createPort(String containerPort) {
        return DockerContainerPort.builder().containerPort(containerPort).hostPort("8080").hostIp("0.0.0.0").protocol("tcp").build();
    }

    private DockerContainerNetwork createNetwork(String networkName) {
        return DockerContainerNetwork.builder().id("123").name(networkName).gateway("192.168.0.1").containerIpAddress("192.168.0.19").driver("bridge").build();
    }
}
