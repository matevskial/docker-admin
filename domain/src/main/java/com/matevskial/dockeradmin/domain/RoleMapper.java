package com.matevskial.dockeradmin.domain;

import com.matevskial.dockeradmin.api.RoleCreateData;
import com.matevskial.dockeradmin.api.RoleData;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleData toData(RoleEntity role);
    RoleEntity toEntity(RoleCreateData role);
}
