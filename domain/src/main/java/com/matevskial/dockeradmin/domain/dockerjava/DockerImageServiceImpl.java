package com.matevskial.dockeradmin.domain.dockerjava;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.SearchItem;
import com.matevskial.dockeradmin.api.docker.DockerImage;
import com.matevskial.dockeradmin.api.docker.DockerImageSearchItem;
import com.matevskial.dockeradmin.api.docker.DockerImageService;
import com.matevskial.dockeradmin.api.docker.filter.DockerImageFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Profile("!stub")
@Slf4j
@RequiredArgsConstructor
public class DockerImageServiceImpl implements DockerImageService {

    // TODO: Extract into spring properties
    private static final int MAX_SEARCH_IMAGE_RESULT_LIMIT = 100;

    private final DockerClient dockerClient;
    private final DockerImageMapper dockerImageMapper;

    @Override
    public List<DockerImage> getImages(DockerImageFilter filter) {
        log.info("Getting images");
        return dockerClient.listImagesCmd().exec().stream()
                .map(image -> dockerClient.inspectImageCmd(image.getId()).exec())
                .map(dockerImageMapper::map)
                .filter(dockerImage -> doFilter(dockerImage, filter))
                .collect(Collectors.toList());
    }

    private boolean doFilter(DockerImage dockerImage, DockerImageFilter filter) {
        return !StringUtils.hasText(filter.getRepoTag()) || dockerImage.getRepoTag().contains(filter.getRepoTag());
    }

    @Override
    public List<DockerImageSearchItem> searchImages(String searchTerm) {
        log.info("Searching images with term {}", searchTerm);
        List<SearchItem> imageSearchItems =  dockerClient.searchImagesCmd(searchTerm).withLimit(MAX_SEARCH_IMAGE_RESULT_LIMIT).exec();
        return dockerImageMapper.mapToDockerImageSearchItems(imageSearchItems);
    }
}
