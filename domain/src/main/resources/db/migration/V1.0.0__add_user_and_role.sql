create table user
(
    id               varchar(36) not null primary key,
    username         varchar(255),
    encoded_password varchar(255)
);

create table role
(
    id   varchar(36) not null primary key,
    name varchar(20) not null unique
);

create table user_role
(
    user_id varchar(36),
    role_id varchar(36),
    primary key (user_id, role_id),
    foreign key(user_id) references user(id),
    foreign key(role_id) references role(id)
);
