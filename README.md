# Docker-admin - A web application for managing docker instances

Docker-admin is built as part of my graduate thesis, using Spring Boot and Vaadin.
The application uses ```docker-admin``` to interface with the docker instance.

## Feature highlights

* UI for managing containers, images, volumes and networks
* Search images on docker-hub
* Duplicate a container configuration in order to create and run new container, or replace existing container
* Retrieve the "docker run" command as accurate as possible for existing containers

### Docker containers dashboard
![Docker containers dashboard](docs/screenshots/docker-containers-dashboard.png)

### Managing Docker Images
![Managing Docker Images](docs/screenshots/managing-docker-images.png)

### Searching Docker-Hub images
![Searching Docker-Hub images](docs/screenshots/searching-docker-hub-images.png)

### Managing Docker Volumes
![Managing Docker Volumes](docs/screenshots/managing-docker-volumes.png)

### Managing Docker Networks
![Managing Docker Networks](docs/screenshots/managing-docker-networks.png)

### Displaying Docker engine Info
![Displaying Docker engine Info](docs/screenshots/displaying-docker-engine-info.png)

## Setup for development

First clone the repository or download and extract archived source code

To open and run the project with IntelliJ IDEA, Use ```File -> Open``` and then select the project folder.

The application requires Java 17+.

Before running the application, execute [clean, install] maven lifecycle, either using IntelliJ or command line
to build the project

* unix(linux and mac)
```./mvnw clean install```

* Windows
```mvnw.cmd clean install```

This will also build the vaadin frontend(i.e generate the javascript) in DEVELOPMENT mode if it is not already built.
You can see ```node_modules```, ```package.json``` and other vaadin-related frontend files being 
generated in the module vaadin-frontend.

To build in production mode, please use the ```production``` maven profile

* unix(linux and mac)

  ```./mvnw clean install -Pproduction```

* Windows

  ```mvnw.cmd clean install -Pproduction```

After building the application, you can start it using the spring profile ```local```, either with IntelliJ, 
or from command line

```java -jar -Dspring.profiles.active=local application/target/application-<current-version>-SHAPSHOT.jar```

If you don't have a docker engine installed on your system, you can use the additional spring profile ```stub```
in order to work with dummy docker objects, instead of connecting to a real docker engine.

```java -jar -Dspring.profiles.active=local,stub application/target/application-<current-version>-SHAPSHOT.jar```

For logging into the application, a default user is created when the application is starting. 

The default username is ```matevskial``` and default password is ```matevskial```.
